#ifndef __QUADBOXFILE__
#define __QUADBOXFILE__

#include <gl/glew.h>
#include <gl/gl.h>

// cubo ///////////////////////////////////////////////////////////////////////
//    v6----- v5
//   /|      /|
//  v1------v0|
//  | |     | |
//  | |v7---|-|v4
//  |/      |/
//  v2------v3

// Coordenadas del vertex array  =====================================
// Un cubo tiene 6 lados y cada lado tiene 2 triangles, por tanto, un cubo
// tiene 36 v�rtices (6 lados * 2 trian * 3 vertices = 36 vertices). Y cada
// vertice tiene 4 components (x,y,z) de reales, por tanto, el tama�o del vertex
// array es 144 floats (36 * 4 = 144).

const unsigned int cubeQuadsNumVerts = 4;
const unsigned int cubeQuadsNumFaces = 1;

float cubeQuadsVertices[] = {
	1, 1, 1,  -1, 1, 1,  -1,-1, 1,   1,-1, 1,    // v0-v1-v2-v3 (front)

//1, 1, 1,   1,-1, 1,   1,-1,-1,   1, 1,-1,    // v0-v3-v4-v5 (right)
//
//1, 1, 1,   1, 1,-1,  -1, 1,-1,  -1, 1, 1,    // v0-v5-v6-v1 (top)
//
//-1, 1, 1,  -1, 1,-1,  -1,-1,-1,  -1,-1, 1,    // v1-v6-v7-v2 (left)
//
//-1,-1,-1,   1,-1,-1,   1,-1, 1,  -1,-1, 1,    // v7-v4-v3-v2 (bottom)
//
//1,-1,-1,  -1,-1,-1,  -1, 1,-1,   1, 1,-1 
};  // v4-v7-v6-v5 (back)


float cubeQuadsNormals[] = {
	0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,   // v0-v1-v2-v3 (front)

//1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,   // v0-v3-v4-v5 (right)
//
//0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,   // v0-v5-v6-v1 (top)
//
//-1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,   // v1-v6-v7-v2 (left)
//
//0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0,   // v7-v4-v3-v2 (bottom)
//
//0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1 
}; // v4-v7-v6-v5 (back)

unsigned int cubeQuadsIndices[] =
{
	0, 1, 2, 3
};


///////////////////////////////////////////////////////////////////////////////
// Init Cube
///////////////////////////////////////////////////////////////////////////////
//void initCube()
//{
//	GLuint vboHandle;
//
//	glGenVertexArrays(1, &cubeVAOHandle);
//	glBindVertexArray(cubeVAOHandle);
//
//	glGenBuffers(1, &vboHandle);
//	glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeQuadsVertices) + sizeof(cubeQuadsNormals), NULL, GL_STATIC_DRAW);
//	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(cubeQuadsVertices), cubeQuadsVertices);
//	glBufferSubData(GL_ARRAY_BUFFER, sizeof(cubeQuadsVertices), sizeof(cubeQuadsNormals), cubeQuadsNormals);
//
//	GLuint loc = glGetAttribLocation(programID, "aPosition");
//	glEnableVertexAttribArray(loc);
//	glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, (char *)NULL + 0);
//	GLuint loc2 = glGetAttribLocation(programID, "aNormal");
//	glEnableVertexAttribArray(loc2);
//	glVertexAttribPointer(loc2, 3, GL_FLOAT, GL_FALSE, 0, (char *)NULL + sizeof(cubeQuadsVertices));
//
//	glPatchParameteri(GL_PATCH_VERTICES, 4);
//
//	glBindVertexArray(0);
//}

#endif