#include "OGLProgramPP.h"
#include "PostProcess.h"

OGLProgramPP::OGLProgramPP(){}

inline GLuint OGLProgramPP::loadShader(const char *fileName, GLenum type)
{
	unsigned int fileLen;
	char *source = loadStringFromFile(fileName, fileLen);

	//////////////////////////////////////////////
	//Creaci�n y compilaci�n del Shader
	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1,//El 1 define en cu�ntas cadenas est� el shader
		(const GLchar **)&source, (const GLint *)&fileLen);
	glCompileShader(shader);
	delete[] source;

	//Comprobamos que se compil� bien
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, NULL, logString);
		std::cout << "Error in" << fileName << ": " << logString << std::endl;
		delete[] logString;
		glDeleteShader(shader);
		exit(-1);
	}

	return shader;
}


OGLProgramPP::OGLProgramPP(const char * vShaderPath, const char * fShaderPath)
{
	vShader = loadShader(vShaderPath, GL_VERTEX_SHADER);
	fShader = loadShader(fShaderPath, GL_FRAGMENT_SHADER);

	program = glCreateProgram();
	glAttachShader(program, vShader);
	glAttachShader(program, fShader);
	glBindAttribLocation(program, 0, "inPos");
	glLinkProgram(program);
	int linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(program, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete logString;
		glDeleteProgram(program);
		program = 0;
		exit(-1);
	}
	uColorTexPP = glGetUniformLocation(program, "colorTex");
	uVertexTexPP = glGetUniformLocation(program, "vertexTex");
	uDepthTexPP = glGetUniformLocation(program, "depthTex");
	uFocalDistance = glGetUniformLocation(program, "focalDistance");
	uMaxDistanceFactor = glGetUniformLocation(program, "maxDistanceFactor");
	uFarPlane = glGetUniformLocation(program, "farPlane");
	uNearPlane = glGetUniformLocation(program, "nearPlane");
	uConvolutionMask = glGetUniformLocation(program, "mask");

	inPosPP = glGetAttribLocation(program, "inPos");
}

void OGLProgramPP::RenderFunc(PostProcess * postProcess)
{
	glUseProgram(program);

	if (uColorTexPP != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, postProcess->colorBuffTexId);
		glUniform1i(uColorTexPP, 0);
	}

	if (uVertexTexPP != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, postProcess->vertexBuffTexId);
		glUniform1i(uVertexTexPP, 1);
	}

	if (uDepthTexPP != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 2);
		glBindTexture(GL_TEXTURE_2D, postProcess->depthBuffTexId);
		glUniform1i(uDepthTexPP, 2);
	}

	if (uFocalDistance != -1)
	{
		glUniform1f(uFocalDistance, postProcess->focalDistance);
	}

	if (uMaxDistanceFactor != -1)
	{
		glUniform1f(uMaxDistanceFactor, postProcess->maxDistanceFactor);
	}

	if (uNearPlane != -1)
	{
		glUniform1f(uNearPlane, postProcess->nearPlane);
	}

	if (uFarPlane != -1)
	{
		glUniform1f(uFarPlane, postProcess->farPlane);
	}
	
	if (uConvolutionMask != -1)
	{
		glUniform1fv(uConvolutionMask, MASK_SIZE, convMatrix);
	}

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void OGLProgramPP::AttachPlane(unsigned int atribID)
{
	glVertexAttribPointer(atribID, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(atribID);
}

void OGLProgramPP::destroy()
{
	glDetachShader(program, vShader);
	glDetachShader(program, fShader);
	glDeleteShader(vShader);
	glDeleteShader(fShader);
	glDeleteProgram(program);
}
