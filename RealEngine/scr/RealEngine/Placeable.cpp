#include "Placeable.h"

Placeable::Placeable(vec3 position, vec3 rotation, vec3 scale)
{
	SetTransform(position, rotation, scale);
}

Placeable::Placeable()
{
	SetTransform(vec3(0.0f), vec3(0.0f), vec3(1.0f));
}

vec3 Placeable::GetPosition()
{
	return position;
}

vec3 Placeable::GetRotation()
{
	return rotation;
}

vec3 Placeable::GetScale()
{
	return scale;
}

void Placeable::SetPosition(vec3 position)
{
	this->position = position;
	RecalculateModelMatrix();
}

void Placeable::SetRotation(vec3 rotation)
{
	this->rotation = rotation;
	RecalculateModelMatrix();
}

void Placeable::SetScale(vec3 scale)
{
	this->scale = scale;
	RecalculateModelMatrix();
}

void Placeable::SetTransform(vec3 position, vec3 rotation, vec3 scale)
{
	this->position = position;
	this->rotation = rotation;
	this->scale = scale;
	RecalculateModelMatrix();
}

mat4 Placeable::GetModelMatrix()
{
	return modelMatrix;
}

void Placeable::SetModelMatrix(mat4 newMatrix)
{
	modelMatrix = newMatrix;
}

void Placeable::RecalculateModelMatrix()
{
	modelMatrix = mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, position);
	modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = glm::scale(modelMatrix, scale);
}
