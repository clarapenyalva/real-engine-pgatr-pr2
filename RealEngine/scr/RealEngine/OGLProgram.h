#pragma once

#include "../auxiliar.h"
#include <windows.h>

#include <gl/glew.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h>
#include <iostream>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Object.h"
#include "RendererObject.h"
#include "Light.h"
//#include "Scene.h"
using namespace std;

class Scene; //Forward declaration

class OGLProgram
{
	/*Cosas que debería tener:
	Variables de identificación de los diferentes elementos del shader
	Generación del programa de OpenGl
	*/

public:


	//Atributos
	int inPos;
	int inColor;
	int inNormal;
	int inTexCoord;

	//Variables Uniform
	int uModelViewMat;
	int uModelViewProjMat;
	int uNormalMat;
	int uProjectionMat;
	//int uDiffuseIntensity;

	//Texturas Uniform
	int uColorTex;
	int uEmiTex;
	int uSpecTex;
	int uNormalTex;
	int uDisplTex;

	//Control práctica 2 PGATR
	int locTessLevelInner;
	int locTessLevelOuter;
	int locDispl;
	int locLodMinTessLevel;
	int locLodMaxTessLevel;

	const float uMinTessLevel = 1.0;
	const float uMaxTessLevel = 10.0;

	OGLProgram() {};
	OGLProgram(const char* vShaderPath, const char*  fShaderPath,
		const char* gShaderPath = NULL, const char* tCShaderPath = NULL, const char* tEShaderPath = NULL);
	//void InitObject(Object obj);
	virtual void RenderFunc(Camera *cam, Scene *context);

	virtual void LoadObject(Object *obj);
	virtual void destroy();

private:
	int uDirectionalLightIntensity;
	int uDirectionalLightDirection;

	int uSpotLightPosition;
	int uSpotLightIntensity;
	int uSpotLightAttenuation;
	int uSpotLightDirection;
	int uSpotLightAngle;

	//Vectores de valores uniform para cada tipo de luz
	/*int uDotLightsPositions;
	int uDotLightsIntensities;
	int uDotLightsAttenuations;

	int uDirectionalLightsIntensities;
	int uDirectionalLightsDirections;

	int uSpotLightsPositions;
	int uSpotLightsIntensities;
	int uSpotLightsAttenuations;
	int uSpotLightsDirections;
	int uSpotLightsAngles;*/

	//RendererObjects que usa este programa

	void UpdateLights(Light dotLight, DirectionalLight directionalLight, SpotLight spotLight, mat4 viewMatrix);
	//void UpdateLights(vector<Light> dotLights, vector<DirectionalLight> directionalLights, vector<SpotLight> spotLights, mat4 viewMatrix);

	void UpdateTessLevel(float tessLevelInner, float tessLevelOuter);
	void UpdateLodTessLevel(float minTessLevel, float maxTessLevel);
	void UpdateDispl(float displacement);


protected:
	unsigned int vShader = -1;
	unsigned int fShader = -1;
	unsigned int gShader = -1;
	unsigned int tCShader = -1;
	unsigned int tEShader = -1;
	unsigned int program;

	vector<RendererObject> rendObjs;

	GLuint loadShader(const char *fileName, GLenum type);
};
