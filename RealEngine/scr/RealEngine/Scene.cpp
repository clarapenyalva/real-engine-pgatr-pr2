#include "Scene.h"

Scene::Scene(vector<Object> objects, DirectionalLight dirLight, Light dotLight, 
	SpotLight spotLight, Camera camera, vec3 ambientalLightIntensity)
{
	this->ambientalLightIntensity = ambientalLightIntensity;
	this->objects = objects;
	this->camera = camera;
	this->directionalLight = dirLight;
	this->dotLight = dotLight;
	this->spotLight = spotLight;

	this->tessLevelInner = 5.0f;
	this->tessLevelOuter = 5.0f;
	this->displacementAmount = 0.5f;
	this->lodMinTessLevel = 1;
	this->lodMaxTessLevel = 10;
	InitializeScene();
}

Scene::Scene()
{

}

/*Scene::Scene(vector<Object> objects, vector<DirectionalLight> dirLights, vector<Light> dotLights, vector<SpotLight> spotLights, Camera camera, vec3 ambientalLightIntensity)
{
this->ambientalLightIntensity = ambientalLightIntensity;
this->objects = objects;
this->camera = camera;
this->directionalLights = dirLights;
this->dotLights = dotLights;
this->spotLights = spotLights;
InitializeScene();
}*/

void Scene::InitializeScene()
{
	/*
	* Load & initialize programs with objects, lights and camera
	*
	* For now, the architecture supports a static object scene (or one where objects
	* are only removed). If objects could be inserted in mid-scene,
	* the map should be kept as a way of checking program non-redundancy.
	*/

	//OBJECTS
	map<int, OGLProgram> programMap;
	map<int, OGLProgram>::iterator it;

	for (size_t i = 0; i < objects.size(); i++)
	{
		hash<string> programHasher;
		int programHash =
			programHasher(objects[i].shaderPaths.vertShader 
				+ objects[i].shaderPaths.fragShader 
				+ objects[i].shaderPaths.geomShader
				+ objects[i].shaderPaths.tessCtrlShader
				+ objects[i].shaderPaths.tessEvShader);

		it = programMap.find(programHash);

		if (it == programMap.end())
		{
			OGLProgram newProgram(objects[i].shaderPaths.vertShader.c_str(), 
				objects[i].shaderPaths.fragShader.c_str(),
				!objects[i].shaderPaths.geomShader.empty() ? objects[i].shaderPaths.geomShader.c_str(): NULL,
				!objects[i].shaderPaths.tessCtrlShader.empty() ? objects[i].shaderPaths.tessCtrlShader.c_str() : NULL,
				!objects[i].shaderPaths.tessEvShader.empty() ? objects[i].shaderPaths.tessEvShader.c_str() : NULL);
			//programs.push_back(newProgram);
			newProgram.LoadObject(&objects[i]);

			programMap.insert(pair<int, OGLProgram>(programHash, newProgram));

		}
		else //If there exists already a program 
		{
			it->second.LoadObject(&objects[i]);
		}
	}

	programs = vector<OGLProgram>();
	programs.reserve(programMap.size());

	for (map<int, OGLProgram>::iterator iter = programMap.begin(); iter != programMap.end(); iter++)
	{
		programs.push_back(iter->second);
	}

	//LIGHTS
	//Lights are not initialized, but loaded as uniforms on each Program render func

	//CAMERA
	//Camera is not initialized, but its values changed and loaded on each Program render func
}

void Scene::RenderScene()
{
	/*
	* Sube info de matrices, luces, etc.
	* Renderiza cada uno de los objetos de cada uno de los programas
	*/

	for (size_t i = 0; i < programs.size(); i++)
	{
		programs[i].RenderFunc(&camera, this);
	}
}

void Scene::destroy()
{
	for (size_t i = 0; i < programs.size(); i++)
	{
		programs[i].destroy();
	}
}