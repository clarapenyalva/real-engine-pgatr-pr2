#pragma once

//#include "OGLProgram.h"
#include "Object.h"
#include "Camera.h"

using namespace std;

class OGLProgram;//FORWARD DECLARATION

class RendererObject
{
public:
	struct TexturesIDs
	{
		unsigned int colorTexId;
		unsigned int emiTexId;
		unsigned int specTexId;
		unsigned int normalTexId;
		unsigned int displTexID;
	};

	Object *obj;
	OGLProgram *parentProgram;

	RendererObject(Object *obj, OGLProgram *parentProgram);
	RendererObject();
	void Render(Camera *cam, OGLProgram *parentProgram);

	void destroy();

private:
	//Texturas
	/*int colorTexId;
	int emiTexId;
	int specTexId;
	int normalTexId;
	int displTexID;*/

	TexturesIDs texturesIDs;

	//VAO
	unsigned int vao;

	//VBOs que forman parte del objeto
	unsigned int posVBO;
	unsigned int colorVBO;
	unsigned int normalVBO;
	unsigned int texCoordVBO;
	unsigned int triangleIndexVBO;

	int LoadTex(string fileName);
	//unsigned int LoadTex(const char * fileName);
};

