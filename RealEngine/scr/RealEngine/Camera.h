#pragma once

//#include "Placeable.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

const float PI = 3.141593f;

class Camera /*: Placeable*///Por ahora Camera es el ni�o especial
{
public:
	Camera(vec3 position, vec3 forward, vec3 up,
		float nearPlane, float farPlane, float aspectRatio, float fovDegrees);

	//Camera(vec3 position, vec3 rotations,
	//	float nearPlane, float farPlane, float aspectRatio, float fovDegrees);

	Camera();

	mat4 GetView();
	void SetView(mat4 newView);
	mat4 GetProjection();

	void SetCamera(vec3 position, vec3 forward, vec3 up);	
	//void SetCamera(vec3 position, vec3 rotations);

	void SetPosition(vec3 position);
	vec3 GetPosition();

	//vec3 GetRotation();
	//void SetRotation(vec3 rotations);

	void SetForward(vec3 forward);
	vec3 GetForward();
	vec3 GetRight();
	void SetUpVector(vec3 up);
	void SetProjection(float nearPlane, float farPlane, float aspectRatio, float fovDegrees);
	void SetProjection(mat4 newProjection);
	void SetFar(float farPlane);
	void SetNear(float nearPlane);
	void SetAspectRatio(float aspectRatio);
	void SetFOV(float fovDegrees);

private:
	float farPlane;
	float nearPlane;
	float aspectRatio;//Width/Height
	float fovDegrees;

	vec3 position;
	vec3 forward;
	vec3 up;
	//vec3 rotations;

	mat4 viewMatrix;
	mat4 projectionMatrix;
};