#include "OGLProgram.h"
#include "Scene.h"

inline void LoadUniformVec3(int identifier, vec3 vec)
{
	if (identifier != -1)
	{
		glUniform3fv(identifier, 1, &(vec[0]));
	}
}

inline void LoadUniform1f(int identifier, float value)
{
	if (identifier != -1)
	{
		glUniform1f(identifier, value);
	}
}


inline void LoadUniformVec3Vector(int identifier, vector<float> vect)
{
	if (identifier != -1)
	{
		glUniform3fv(identifier, vect.size(), &(vect[0]));
	}
}

OGLProgram::OGLProgram(const char* vShaderPath, const char*  fShaderPath, const char* gShaderPath, const char* tCShaderPath, const char* tEShaderPath)
{
	vShader = loadShader(vShaderPath, GL_VERTEX_SHADER);
	fShader = loadShader(fShaderPath, GL_FRAGMENT_SHADER);

	if (gShaderPath != NULL)	
		gShader = loadShader(gShaderPath, GL_GEOMETRY_SHADER);

	if (tCShaderPath != NULL)
		tCShader = loadShader(tCShaderPath, GL_TESS_CONTROL_SHADER);

	if (tEShaderPath != NULL)
		tEShader = loadShader(tEShaderPath, GL_TESS_EVALUATION_SHADER);
	

	program = glCreateProgram();
	glAttachShader(program, vShader);
	glAttachShader(program, fShader);
	if (gShader != -1) glAttachShader(program, gShader);
	if (tCShader != -1) glAttachShader(program, tCShader);
	if (tEShader != -1) glAttachShader(program, tEShader);

	glBindAttribLocation(program, 0, "inPos");//Si en el programa hay una variable inPos, as�gnale el 0
	glBindAttribLocation(program, 1, "inColor");
	glBindAttribLocation(program, 2, "inNormal");
	glBindAttribLocation(program, 3, "inTexCoord");

	glLinkProgram(program);

	int linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(program, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete[] logString;
		glDeleteProgram(program);
		program = 0;
		exit(-1);
	}

	uNormalMat = glGetUniformLocation(program, "normal");
	uModelViewMat = glGetUniformLocation(program, "modelView");
	uModelViewProjMat = glGetUniformLocation(program, "modelViewProj");
	uProjectionMat = glGetUniformLocation(program, "projection");
	//uDiffuseIntensity = glGetUniformLocation(program, "Id");

	uDirectionalLightIntensity = glGetUniformLocation(program, "directionalLightIntensity");
	uDirectionalLightDirection = glGetUniformLocation(program, "directionalLightDirection");

	uSpotLightPosition = glGetUniformLocation(program, "spotLightPosition");
	uSpotLightIntensity = glGetUniformLocation(program, "spotLightIntensity");
	uSpotLightAttenuation = glGetUniformLocation(program, "spotLightAttenuation");
	uSpotLightDirection = glGetUniformLocation(program, "spotLightDirection");
	uSpotLightAngle = glGetUniformLocation(program, "spotLightAngle");

	/*uDotLightsPositions = glGetUniformLocation(program, "dotLightPositions");
	uDotLightsIntensities = glGetUniformLocation(program, "dotLightIntensities");
	uDotLightsAttenuations = glGetUniformLocation(program, "dotLightAttenuations");

	uDirectionalLightsIntensities = glGetUniformLocation(program, "directLightsIntensities");
	uDirectionalLightsDirections = glGetUniformLocation(program, "directLightsDirections");

	uSpotLightsPositions = glGetUniformLocation(program, "spotLightPositions");
	uSpotLightsIntensities = glGetUniformLocation(program, "spotLightIntensites");
	uSpotLightsAttenuations = glGetUniformLocation(program, "spotLightAttenuations");
	uSpotLightsDirections = glGetUniformLocation(program, "spotLightDirections");
	uSpotLightsAngles = glGetUniformLocation(program, "spotLightAngles");*/

	inPos = glGetAttribLocation(program, "inPos");
	inColor = glGetAttribLocation(program, "inColor");
	inNormal = glGetAttribLocation(program, "inNormal");
	inTexCoord = glGetAttribLocation(program, "inTexCoord"); //inTexCoord va a ser -1 porque no se usa en shader.v0.frag, aunque si se use en el vert

	uColorTex = glGetUniformLocation(program, "colorTex");
	uEmiTex = glGetUniformLocation(program, "emiTex");
	uSpecTex = glGetUniformLocation(program, "specTex");
	uNormalTex = glGetUniformLocation(program, "normalTex");
	uDisplTex = glGetUniformLocation(program, "displTex");

	locTessLevelInner = glGetUniformLocation(program, "uTessLevelInner");
	locTessLevelOuter = glGetUniformLocation(program, "uTessLevelOuter");
	locDispl = glGetUniformLocation(program, "uDisplacementAmount");
	locLodMinTessLevel = glGetUniformLocation(program, "uLodMinTessLevel");
	locLodMaxTessLevel = glGetUniformLocation(program, "uLodMaxTessLevel");
}
/**/
void OGLProgram::RenderFunc(Camera *cam, Scene *context)
{
	//Usar programa
	glUseProgram(program);

	//We could just update the necessary lights when they are changed, but for now this remains here
	UpdateLights(context->dotLight, context->directionalLight, context->spotLight, cam->GetView());

	//Update all the uniforms
	UpdateTessLevel(context->tessLevelInner, context->tessLevelOuter);
	UpdateDispl(context->displacementAmount);
	UpdateLodTessLevel(context->lodMinTessLevel, context->lodMaxTessLevel);




	//Render
	for (size_t i = 0; i < rendObjs.size(); i++)
	{
		rendObjs[i].Render(cam, this);
	}
}
//*/

inline GLuint OGLProgram::loadShader(const char *fileName, GLenum type)
{
	unsigned int fileLen;
	char *source = loadStringFromFile(fileName, fileLen);

	//////////////////////////////////////////////
	//Creaci�n y compilaci�n del Shader
	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1,//El 1 define en cu�ntas cadenas est� el shader
		(const GLchar **)&source, (const GLint *)&fileLen);
	glCompileShader(shader);
	delete[] source;

	//Comprobamos que se compil� bien
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, NULL, logString);
		std::cout << "Error in" << fileName << ": " << logString << std::endl;
		delete[] logString;
		glDeleteShader(shader);
		exit(-1);
	}

	return shader;
}

void OGLProgram::LoadObject(Object *obj)
{
	rendObjs.push_back(RendererObject(obj, this));
}

void OGLProgram::UpdateLights(Light dotLight, DirectionalLight directionalLight, SpotLight spotLight, mat4 viewMatrix)
{
	mat4 modelViewLight = viewMatrix * spotLight.GetModelMatrix();

	vec3 viewLightDirection =
		vec3(modelViewLight * vec4(directionalLight.direction, 0));


	LoadUniformVec3(uDirectionalLightIntensity, directionalLight.lightIntensity);
	LoadUniformVec3(uDirectionalLightDirection, viewLightDirection);

	vec3 lightPos = vec3(modelViewLight * vec4(0, 0, 0, 1));

	viewLightDirection =
		vec3(modelViewLight * vec4(spotLight.direction, 0));

	LoadUniformVec3(uSpotLightPosition, lightPos);
	LoadUniformVec3(uSpotLightIntensity, spotLight.lightIntensity);
	LoadUniformVec3(uSpotLightAttenuation, spotLight.attenuationConstants);
	LoadUniformVec3(uSpotLightDirection, viewLightDirection);
	if (uSpotLightAngle != -1)
	{
		glUniform1f(uSpotLightAngle, spotLight.angle);
	}

}

void OGLProgram::UpdateTessLevel(float tessLevelInner, float tessLevelOuter)
{
	if (locTessLevelInner != -1)
	{
		glUniform1f(locTessLevelInner, tessLevelInner);
	}
	
	if (locTessLevelOuter != -1)
	{
		glUniform1f(locTessLevelOuter, tessLevelOuter);
	}
}

void OGLProgram::UpdateLodTessLevel(float minTessLevel, float maxTessLevel)
{
	if (locLodMinTessLevel != -1)
	{
		glUniform1f(locLodMinTessLevel, minTessLevel);
	}

	if (locLodMaxTessLevel != -1)
	{
		glUniform1f(locLodMaxTessLevel, maxTessLevel);
	}
}

void OGLProgram::UpdateDispl(float displacement)
{
	if (locDispl != -1)
	{
		glUniform1f(locDispl, displacement);
	}
}

/**
void OGLProgram::UpdateLights(vector<Light> dotLights, vector<DirectionalLight> directionalLights, vector<SpotLight> spotLights, mat4 viewMatrix)
{
//We can upload empty vectors to the shader, as it is simply going to iterate with
//0 iterations on that given array

vector<float> dotLightsPositions(dotLights.size() * 3);
vector<float> dotLightsIntensities(dotLights.size() * 3);
vector<float> dotLightsAttenuations(dotLights.size() * 3);

vector<float> directionalLightsIntensities(directionalLights.size() * 3);
vector<float> directionalLightsDirections(directionalLights.size() * 3);

vector<float> spotLightsPositions(spotLights.size() * 3);
vector<float> spotLightsIntensities(spotLights.size() * 3);
vector<float> spotLightsAttenuations(spotLights.size() * 3);
vector<float> spotLightsDirections(spotLights.size() * 3);
vector<float> spotLightsAngles(spotLights.size());

vec3 lightPos;
mat4 modelViewLight(1);

vec3 viewLightDirection;

for (size_t i = 0; i < dotLights.size(); i++)
{
modelViewLight = viewMatrix * dotLights[i].GetModelMatrix();
lightPos = vec3(modelViewLight * vec4(0, 0, 0, 1));

dotLightsPositions[3 * i] = lightPos.x;
dotLightsIntensities[3 * i] = dotLights[i].lightIntensity.x;
dotLightsAttenuations[3 * i] = dotLights[i].attenuationConstants.x;

dotLightsPositions[3 * i + 1] = lightPos.y;
dotLightsIntensities[3 * i + 1] = dotLights[i].lightIntensity.y;
dotLightsAttenuations[3 * i + 1] = dotLights[i].attenuationConstants.y;

dotLightsPositions[3 * i + 2] = lightPos.z;
dotLightsIntensities[3 * i + 2] = dotLights[i].lightIntensity.z;
dotLightsAttenuations[3 * i + 2] = dotLights[i].attenuationConstants.z;
}

for (size_t i = 0; i < directionalLights.size(); i++)
{
viewLightDirection = vec3(viewMatrix * directionalLights[i].GetModelMatrix() * vec4(directionalLights[i].direction, 0));

directionalLightsIntensities[3 * i] = directionalLights[i].lightIntensity.x;
directionalLightsDirections[3 * i] = viewLightDirection.x;

directionalLightsIntensities[3 * i + 1] = directionalLights[i].lightIntensity.y;
directionalLightsDirections[3 * i + 1] = viewLightDirection.y;

directionalLightsIntensities[3 * i + 2] = directionalLights[i].lightIntensity.z;
directionalLightsDirections[3 * i + 2] = viewLightDirection.z;
}

for (size_t i = 0; i < spotLights.size(); i++)
{
viewLightDirection = vec3(viewMatrix * spotLights[i].GetModelMatrix() * vec4(spotLights[i].direction, 0));

modelViewLight = viewMatrix * spotLights[i].GetModelMatrix();
lightPos = (vec3)(modelViewLight * vec4(0, 0, 0, 1));

spotLightsAngles[i] = spotLights[i].angle;

spotLightsPositions[3 * i] = lightPos.x;
spotLightsIntensities[3 * i] = spotLights[i].lightIntensity.x;
spotLightsAttenuations[3 * i] = spotLights[i].attenuationConstants.x;
spotLightsDirections[3 * i] = viewLightDirection.x;

spotLightsPositions[3 * i + 1] = lightPos.y;
spotLightsIntensities[3 * i + 1] = spotLights[i].lightIntensity.y;
spotLightsAttenuations[3 * i + 1] = spotLights[i].attenuationConstants.y;
spotLightsDirections[3 * i + 1] = viewLightDirection.y;

spotLightsPositions[3 * i + 2] = lightPos.z;
spotLightsIntensities[3 * i + 2] = spotLights[i].lightIntensity.z;
spotLightsAttenuations[3 * i + 2] = spotLights[i].attenuationConstants.z;
spotLightsDirections[3 * i + 2] = viewLightDirection.z;
}

LoadUniformVec3Vector(uDotLightsPositions, dotLightsPositions);
LoadUniformVec3Vector(uDotLightsIntensities, dotLightsIntensities);
LoadUniformVec3Vector(uDotLightsAttenuations, dotLightsAttenuations);

LoadUniformVec3Vector(uDirectionalLightsIntensities, directionalLightsIntensities);
LoadUniformVec3Vector(uDirectionalLightsDirections, directionalLightsDirections);

LoadUniformVec3Vector(uSpotLightsPositions, spotLightsPositions);
LoadUniformVec3Vector(uSpotLightsIntensities, spotLightsIntensities);
LoadUniformVec3Vector(uSpotLightsAttenuations, spotLightsAttenuations);
LoadUniformVec3Vector(uSpotLightsDirections, spotLightsDirections);

if (uSpotLightsAngles != -1)
{
glUniform1fv(uSpotLightsAngles, spotLightsAngles.size(), &(spotLightsAngles[0]));
}
}

// */


void OGLProgram::destroy()
{
	glDetachShader(program, vShader);
	glDeleteShader(vShader);

	glDetachShader(program, fShader);
	glDeleteShader(fShader);

	if (gShader != -1)
	{
		glDetachShader(program, gShader);
		glDeleteShader(gShader);
	}

	glDeleteProgram(program);

	//for each rendererobject, destroy it
	for (size_t i = 0; i < rendObjs.size(); i++)
	{
		rendObjs[i].destroy();
	}
}
