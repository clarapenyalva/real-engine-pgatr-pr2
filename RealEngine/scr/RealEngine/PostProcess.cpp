#include "PostProcess.h"

/* Right now this class only supports one post process effect at a time.
* A double switching render texture system should be made to support 
* an arbitrary number of effects.
*/

PostProcess::PostProcess(unsigned int w, unsigned int h)
{
	gaussianBlur = OGLProgramPP(vShader, fShaderGaussian);
	depthOfField = OGLProgramPP(vShader, fShaderDOF);

	initPlane();

	glGenFramebuffers(1, &fbo);
	glGenTextures(1, &colorBuffTexId);
	glGenTextures(1, &depthBuffTexId);
	glGenTextures(1, &vertexBuffTexId);

	glGenFramebuffers(1, &fbo2);
	glGenTextures(1, &colorBuffTexId2);
	glGenTextures(1, &depthBuffTexId2);
	glGenTextures(1, &vertexBuffTexId2);

	resizeFBO(w, h);
	//Motion Blur requires no initialization

	currentConvolution = BLUR;
	depthOfField.convMatrix = &(blurConvolutionMatrix[0]);
}

void PostProcess::SwitchConvMatrix()
{
	currentConvolution = (ConvolutionType)((currentConvolution + 1) % convMatricesAmount);

	switch (currentConvolution)
	{
	case ConvolutionType::BLUR:
		depthOfField.convMatrix = &(blurConvolutionMatrix[0]);
		break;

	case ConvolutionType::SHARPEN:
		depthOfField.convMatrix = &(sharpenConvolutionMatrix[0]);
		break;

	case ConvolutionType::EDGES:
		depthOfField.convMatrix = &(edgesConvolutionMatrix[0]);
		break;

	case ConvolutionType::EMBOSS:
		depthOfField.convMatrix = &(embossConvolutionMatrix[0]);
		break;
	default:
		break;
	}
}

void PostProcess::PreRender()
{
	//throw exception("Function not implemented.");

	//Antes de limpiar el FB
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	hasPreRendered = true;
}

void PostProcess::resizeFBO(unsigned int w, unsigned int h)
{
	//Color buffer
	glBindTexture(GL_TEXTURE_2D, colorBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,
		GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, colorBuffTexId2);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,
		GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	//Depth buffer
	glBindTexture(GL_TEXTURE_2D, depthBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0,
		GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, depthBuffTexId2);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0,
		GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	//Vertext buffer
	glBindTexture(GL_TEXTURE_2D, vertexBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, vertexBuffTexId2);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, colorBuffTexId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
		GL_TEXTURE_2D, vertexBuffTexId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
		depthBuffTexId, 0);

	const GLenum buffs[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, buffs);
	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER))
	{
		std::cerr << "Error configurando el FBO" << std::endl;
		exit(-1);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, colorBuffTexId2, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
		GL_TEXTURE_2D, vertexBuffTexId2, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
		depthBuffTexId2, 0);

	const GLenum buffs2[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, buffs2);
	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER))
	{
		std::cerr << "Error configurando el FBO" << std::endl;
		exit(-1);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PostProcess::Render()
{
	if (!hasPreRendered)
		throw exception("Calling Post Process without rendering.");

	//throw exception("Function not implemented.");
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	glBindVertexArray(planeVAO);

	//gaussianBlur.RenderFunc(this);

	//LAST Post Process Program render of the frame----------------------------------
	//We activate at last the screen (default) frame buffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//glUseProgram(gaussianBlur.program);

	//If motion blur is activated, we need to use blending
	if (doMotionBlur)
	{
		glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBlendFunc(GL_CONSTANT_COLOR, GL_CONSTANT_ALPHA);
		//glBlendColor(0.5f, 0.5f, 0.5f, motionBlurValue);
		glBlendColor(motionBlurValue, motionBlurValue, motionBlurValue, 1.0f-motionBlurValue);
		glBlendEquation(GL_FUNC_ADD);
	}

	//gaussianBlur.RenderFunc(this);
	depthOfField.RenderFunc(this);//Post process render

	if (doMotionBlur)
		glDisable(GL_BLEND);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	hasPreRendered = false;
}

void PostProcess::destroy()
{
	glDeleteBuffers(1, &planeVertexVBO);
	glDeleteVertexArrays(1, &planeVAO);

	glDeleteFramebuffers(1, &fbo);
	glDeleteTextures(1, &colorBuffTexId);
	glDeleteTextures(1, &depthBuffTexId);
	glDeleteTextures(1, &vertexBuffTexId);

	glDeleteFramebuffers(1, &fbo2);
	glDeleteTextures(1, &colorBuffTexId2);
	glDeleteTextures(1, &depthBuffTexId2);
	glDeleteTextures(1, &vertexBuffTexId2);

	gaussianBlur.destroy();
	depthOfField.destroy();
}

void PostProcess::initPlane()
{
	glGenVertexArrays(1, &planeVAO);
	glBindVertexArray(planeVAO);

	glGenBuffers(1, &planeVertexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, planeVertexVBO);
	glBufferData(GL_ARRAY_BUFFER, planeNVertex * sizeof(float) * 3,
		planeVertexPos, GL_STATIC_DRAW);

	gaussianBlur.AttachPlane(gaussianBlur.inPosPP);
	depthOfField.AttachPlane(depthOfField.inPosPP);
}
