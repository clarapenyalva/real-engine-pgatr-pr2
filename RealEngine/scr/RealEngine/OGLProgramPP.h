#pragma once
#include "OGLProgram.h";

class PostProcess;

#ifndef MASK_SIZE
#define MASK_SIZE 9u
#endif

class OGLProgramPP //: public OGLProgram
{
public:
	int inPosPP;
	int vShader, fShader, program;
	float * convMatrix;

	OGLProgramPP();
	OGLProgramPP(const char* vShaderPath, const char*  fShaderPath);

	void RenderFunc(PostProcess * context);
	//virtual void destroy() override;
	void destroy();

	void AttachPlane(unsigned int atribID);

private:
	int uColorTexPP;
	int uVertexTexPP;
	int uDepthTexPP;
	int uFocalDistance;
	int uMaxDistanceFactor;
	int uFarPlane;
	int uNearPlane;
	int uConvolutionMask;

	GLuint loadShader(const char *fileName, GLenum type);
	

};