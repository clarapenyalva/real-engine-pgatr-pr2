#include "Light.h"

Light::Light(vec3 lightIntensity, vec3 attCts, vec3 position)
	:Placeable(position, vec3(0), vec3(1))
{
	this->lightIntensity = lightIntensity;
	this->attenuationConstants = attCts;
}

Light::Light()
{
}

DirectionalLight:: DirectionalLight(vec3 lightIntensity, vec3 direction)
	: Light(lightIntensity, vec3(1, 0, 0), vec3(0))
{
	this->direction = direction;
}

DirectionalLight::DirectionalLight()
{
}

SpotLight::SpotLight(vec3 lightIntensity, vec3 attenuationConstants, vec3 direction, float angle, vec3 position)
	: Light(lightIntensity, attenuationConstants, position)
{
	this->direction = direction;
	this->angle = angle;
}

SpotLight::SpotLight()
{
}
