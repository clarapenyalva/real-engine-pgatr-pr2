#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

class Placeable
{
public:
	Placeable(vec3 position, vec3 rotation, vec3 scale);
	Placeable();

	virtual vec3 GetPosition();
	virtual vec3 GetRotation();
	virtual vec3 GetScale();

	virtual void SetPosition(vec3 position);
	virtual void SetRotation(vec3 rotation);
	virtual void SetScale(vec3 scale);

	virtual void SetTransform(vec3 position, vec3 rotation, vec3 scale);
	virtual mat4 GetModelMatrix();
	virtual void SetModelMatrix(mat4 newMatrix);

private:

	void RecalculateModelMatrix();

protected:

	mat4 modelMatrix;
	vec3 position;
	vec3 rotation;
	vec3 scale;
};