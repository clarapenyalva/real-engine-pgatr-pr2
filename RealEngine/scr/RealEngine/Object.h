#pragma once

#include "Placeable.h"
#include <glm/glm.hpp>
#include <vector>
#include <gl/glew.h>

using namespace std;

//typedef  pair<unsigned int*, size_t> cArrayAndSizeI;
//typedef  pair<float*, size_t> cArrayAndSizeF;

struct cArrayAndSizeF
{
	float* arrayData;
	size_t sizeOfArray;
	cArrayAndSizeF() {}
};

struct cArrayAndSizeI
{
	unsigned int* arrayData;
	size_t sizeOfArray;
	cArrayAndSizeI(){}
};

class Object : public Placeable
{
	/*This corresponds to Transform, Mesh and Material in Unity*/
public:
	struct ShaderPaths
	{
		string vertShader;
		string fragShader;
		string geomShader;
		string tessCtrlShader;
		string tessEvShader;

		ShaderPaths(
			string vertShader,
		string fragShader,
		string geomShader,
		string tessCtrlShader,
		string tessEvShader)
		{
			this->vertShader = vertShader;
			this->fragShader = fragShader;
			this->geomShader = geomShader;
			this->tessCtrlShader = tessCtrlShader;
			this->tessEvShader = tessEvShader;
		}


		ShaderPaths(){}
	};

	struct TexturePaths
	{
		string colorTexPath;
		string normalTexPath;
		string specTexPath;
		string emiTexPath;
		string displTexPath;

		TexturePaths(
			string colorTexPath,
			string normalTexPath,
			string specTexPath,
			string emiTexPath,
			string displTexPath)
		{
			this->colorTexPath = colorTexPath;
			this->normalTexPath = normalTexPath;
			this->specTexPath = specTexPath;
			this->emiTexPath = emiTexPath;
			this->displTexPath = displTexPath;
		}
		TexturePaths(){}
	};

	struct MeshInfo
	{
		cArrayAndSizeF vertexPositions;
		cArrayAndSizeF vertexColor;
		cArrayAndSizeF vertexNormals;
		cArrayAndSizeF vertexUVs;
		cArrayAndSizeF vertexTangents;
		cArrayAndSizeI triangles;

		MeshInfo(
			cArrayAndSizeF vertexPositions,
			cArrayAndSizeF vertexColor,
			cArrayAndSizeF vertexNormals,
			cArrayAndSizeF vertexUVs,
			cArrayAndSizeF vertexTangents,
			cArrayAndSizeI triangles)
		{
			this->vertexPositions = vertexPositions;
			this->vertexColor = vertexColor;
			this->vertexNormals = vertexNormals;
			this->vertexUVs = vertexUVs;
			this->vertexTangents = vertexTangents;
			this->triangles = triangles;
		}

		MeshInfo(
			cArrayAndSizeF vertexPositions,
			cArrayAndSizeF vertexNormals,
			cArrayAndSizeI triangles)
		{
			this->vertexPositions = vertexPositions;
			this->vertexNormals = vertexNormals;
			this->triangles = triangles;
		}


		MeshInfo() {}
	};
	//glm::mat4 modelMatrix;

	//Object(cArrayAndSizeI triangleIndex, cArrayAndSizeF cubeVertexPos, cArrayAndSizeF cubeVertexNormal,
	//	cArrayAndSizeF cubeVertexColor, cArrayAndSizeF cubeVertexTexCoord, cArrayAndSizeF cubeVertexTangent,
	//	string colorTexPath, string normalTexPath, string specTexPath, string emiTexPath,
	//	string vertShader, string fragShader, string geomShader, GLenum drawType, vec3 position, vec3 rotation, vec3 scale);

	Object(string meshPath, ShaderPaths shaderPaths, TexturePaths texturePaths, 
		GLenum drawType, GLint dimensionPatch, 
		vec3 position, vec3 rotation, vec3 scale);
	Object(MeshInfo meshInfo, ShaderPaths shaderPaths, TexturePaths texturePaths,
		GLenum drawType, GLint dimensionPatch, 
		vec3 position, vec3 rotation, vec3 scale);

	Object() {}

	/*Object(cArrayAndSizeI triangleIndex, cArrayAndSizeF cubeVertexPos, cArrayAndSizeF cubeVertexNormal,
		string colorTexPath, string normalTexPath,
		string specTexPath, string emiTexPath,
		string vertShader, string fragShader,
		GLenum drawType, GLint dimensionPatch, vec3 position, vec3 rotation, vec3 scale);

	Object(string meshPath, string colorTexPath, string normalTexPath,
		string specTexPath, string emiTexPath,
		string vertShader, string fragShader, string geomShader, string tessCtrlShader, string tessEvShader,
		GLenum drawType, GLint dimensionPatch, vec3 position, vec3 rotation, vec3 scale);

	Object(string meshPath, string colorTexPath, string normalTexPath,
		string specTexPath, string emiTexPath,
		string vertShader, string fragShader, string tessCtrlShader, string tessEvShader,
		GLenum drawType, GLint dimensionPatch, vec3 position, vec3 rotation, vec3 scale);

	Object(string meshPath, string colorTexPath, string normalTexPath,
		string specTexPath, string emiTexPath,
		string vertShader, string fragShader, string geomShader, GLenum drawType,
		vec3 position, vec3 rotation, vec3 scale);

	Object(cArrayAndSizeI triangleIndex, cArrayAndSizeF cubeVertexPos, cArrayAndSizeF cubeVertexNormal,
		string colorTexPath, string normalTexPath, string specTexPath, string emiTexPath,
		string vertShader, string fragShader, string geomShader, string tessCtrlShader, string tessEvShader,
		GLenum drawType, GLint dimensionPatch,
		vec3 position, vec3 rotation, vec3 scale);*/

	/*Object(ArrPairInt triangleIndex, ArrPairFloat cubeVertexPos, ArrPairFloat cubeVertexNormal,
		ArrPairFloat cubeVertexColor, ArrPairFloat cubeVertexTexCoord, ArrPairFloat cubeVertexTangent,
		string colorTexPath, string normalTexPath, string specTexPath, string emiTexPath,
		string vertShader, string fragShader, GLenum drawType, vec3 position, vec3 rotation, vec3 scale);

	Object(string meshPath, string colorTexPath, string normalTexPath, string specTexPath, string emiTexPath,
		string vertShader, string fragShader, GLenum drawType, vec3 position, vec3 rotation, vec3 scale);
*/
	//Per-Vertex Info
	//cArrayAndSizeF vertexPositions;
	//cArrayAndSizeF vertexColor;
	//cArrayAndSizeF vertexNormals;
	//cArrayAndSizeF vertexUVs;
	//cArrayAndSizeF vertexTangents;
	//cArrayAndSizeI triangles;

	MeshInfo meshInfo;
	ShaderPaths shaderPaths;
	TexturePaths texturePaths;

	//Texture PATHS
	//string colorTexPath;
	//string normalTexPath;
	//string specTexPath;
	//string emiTexPath;
	//string displTexPath;

	//Shader PATHS
	/*string vertShader;
	string fragShader;
	string geomShader;
	string tessCtrlShader;
	string tessEvShader;*/

	GLenum drawType;
	GLint dimensionPatch;

private:
	Object(ShaderPaths shaderPaths, TexturePaths texturePaths, GLenum drawType,
		GLint dimensionPatch,
		vec3 position, vec3 rotation, vec3 scale);

	MeshInfo LoadMesh(string path);
};
