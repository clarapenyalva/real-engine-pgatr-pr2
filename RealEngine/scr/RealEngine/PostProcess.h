#pragma once

#include <exception>
#include "OGLProgramPP.h"
#include "../PLANE.h"

using namespace std;
class PostProcess
{
public:
	float motionBlurValue = 0.5f;
	float focalDistance = -25.0f, maxDistanceFactor = 1.0f / 5.0f, nearPlane, farPlane;
	bool doMotionBlur = true;

	PostProcess() {};
	PostProcess(unsigned int w, unsigned int h);

	unsigned int planeVAO;
	unsigned int colorBuffTexId;
	unsigned int depthBuffTexId;
	unsigned int vertexBuffTexId;

	unsigned int colorBuffTexId2;
	unsigned int depthBuffTexId2;
	unsigned int vertexBuffTexId2;

	void SwitchConvMatrix();
	void PreRender();//Bind fbo, etc.
	void Render();
	void resizeFBO(unsigned int w, unsigned int h);
	void destroy();

private:
	bool hasPreRendered = false;

	unsigned int fbo;
	unsigned int fbo2;

	OGLProgramPP gaussianBlur;
	OGLProgramPP depthOfField;

	const char* vShader = "./shaders/postProcessing.v2.vert";
	const char*	fShaderGaussian = "./shaders/postProcessing.v1.frag";
	const char*	fShaderDOF = "./shaders/postProcessing.v2.frag";

	unsigned int planeVertexVBO;

	void initPlane();

	unsigned int convMatricesAmount = 4;
	enum ConvolutionType
	{
		BLUR, SHARPEN, EDGES, EMBOSS
	};

	float blurConvolutionMatrix[MASK_SIZE]{
		float(1.0 / 14.0), float(2.0 / 14.0), float(1.0 / 14.0),
		float(2.0 / 14.0), float(2.0 / 14.0), float(2.0 / 14.0),
		float(1.0 / 14.0), float(2.0 / 14.0), float(1.0 / 14.0) };

	float sharpenConvolutionMatrix[MASK_SIZE]{
		float(0), float(-1.0), float(0),
		float(-1.0), float(5.0), float(-1.0),
		float(0), float(-1.0), float(0) };

	float edgesConvolutionMatrix[MASK_SIZE]{
		float(0), float(1.0), float(0),
		float(1.0), float(-4.0), float(1.0),
		float(0), float(1.0), float(0) };

	float embossConvolutionMatrix[MASK_SIZE]{
		float(-2), float(-1.0), float(0),
		float(-1.0), float(1.0), float(1.0),
		float(0), float(1.0), float(2) };

	ConvolutionType currentConvolution;
};