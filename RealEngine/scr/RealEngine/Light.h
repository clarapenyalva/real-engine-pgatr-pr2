#pragma once

#include "Placeable.h"
using namespace glm;

//Serves as Dot Light
class Light : public Placeable
{
public:
	vec3 lightIntensity; //We use light intensity as diffuse AND specular
	vec3 attenuationConstants;

	Light(vec3 lightIntensity, vec3 attCts, vec3 position);
	Light();
private:

protected:
};

class DirectionalLight : public Light
{
public:
	vec3 direction;

	DirectionalLight(vec3 lightIntensity, vec3 direction);
	DirectionalLight();
private:

protected:
};

class SpotLight : public Light
{
public:
	vec3 direction;
	float angle;

	SpotLight(vec3 lightIntensity, vec3 attenuationConstants, vec3 direction, float angle, vec3 position);
	SpotLight();
private:

protected:
};