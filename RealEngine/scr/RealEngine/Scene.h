#pragma once

#include "Object.h"
#include "Light.h"
#include "Camera.h"
#include "OGLProgram.h"
#include "PostProcess.h"
#include <vector>
#include <map>

using namespace std;
using namespace glm;

class Scene
{
public:
	vec3 ambientalLightIntensity;
	Light dotLight;
	DirectionalLight directionalLight;
	SpotLight spotLight;
	//vector<Light> dotLights;
	//vector<DirectionalLight> directionalLights;
	//vector<SpotLight> spotLights;
	vector<Object> objects;
	Camera camera;

	float tessLevelInner;
	float tessLevelOuter;
	float displacementAmount;
	float lodMinTessLevel;
	float lodMaxTessLevel;

	float displGeometry;
	   
	Scene(vector<Object> objects, vector<Light> lights, Camera camera, vec3 ambientalLightIntensity);
	Scene(vector<Object> objects, DirectionalLight dirLight, Light dotLight, SpotLight spotLight, Camera camera, vec3 ambientalLightIntensity);
	Scene();
	//Scene(vector<Object> objects, vector<DirectionalLight> dirLights, vector<Light> dotLights, vector<SpotLight> spotLights, Camera camera, vec3 ambientalLightIntensity);


	void RenderScene();
	void destroy();

private:
	vector<OGLProgram> programs;

	void InitializeScene();
};

