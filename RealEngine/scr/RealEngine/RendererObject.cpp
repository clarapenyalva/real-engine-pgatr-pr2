#include <gl/glew.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h>
#include <iostream>
#define GLM_FORCE_RADIANS

#include "RendererObject.h"
#include "OGLProgram.h"

void BindTexture(int uTexId, int texId, unsigned int pos)
{
	if (uTexId != -1)
	{
		glActiveTexture(GL_TEXTURE0 + pos);
		glBindTexture(GL_TEXTURE_2D, texId);
		glUniform1i(uTexId, pos);//<-- Esto es lento, se puede hacer directamente en el shader (mirar fragments v1) desde OGL 4.2
	}
}

void LoadBuffer(int bufferIndex, unsigned int &VBO, float* bufferData, size_t bufferSize,
	GLint size, GLenum type, GLboolean normalize) {
	if (bufferIndex!= -1)
	{
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		glBufferData(GL_ARRAY_BUFFER, bufferSize * sizeof(float),//RECUERDA: el tama�o es en bytes
			bufferData, GL_STATIC_DRAW);
		glVertexAttribPointer(bufferIndex, size, type, normalize, 0, 0);//3: vectores de 3 coord, false: no normalizes, 0: stride, 0: comienzo buffers
		glEnableVertexAttribArray(bufferIndex);
	}
}

RendererObject::RendererObject() {}
RendererObject::RendererObject(Object *obj, OGLProgram *parentProgram)
{
	this->obj = obj;
	this->parentProgram = parentProgram;

	//Aqu� se almacenar� la configuraci�n del objeto
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	LoadBuffer(parentProgram->inPos, posVBO, obj->meshInfo.vertexPositions.arrayData, obj->meshInfo.vertexPositions.sizeOfArray, 3, GL_FLOAT, GL_FALSE);
	LoadBuffer(parentProgram->inColor, colorVBO, obj->meshInfo.vertexColor.arrayData, obj->meshInfo.vertexColor.sizeOfArray, 3, GL_FLOAT, GL_FALSE);
	LoadBuffer(parentProgram->inNormal, normalVBO, obj->meshInfo.vertexNormals.arrayData, obj->meshInfo.vertexNormals.sizeOfArray, 3, GL_FLOAT, GL_FALSE);
	LoadBuffer(parentProgram->inTexCoord, texCoordVBO, obj->meshInfo.vertexUVs.arrayData, obj->meshInfo.vertexUVs.sizeOfArray, 2, GL_FLOAT, GL_FALSE);

	//if (parentProgram->inPos != -1)
	//{
	//	glGenBuffers(1, &posVBO);
	//	glBindBuffer(GL_ARRAY_BUFFER, posVBO);

	//	glBufferData(GL_ARRAY_BUFFER, obj->vertexPositions.size() * sizeof(float),//RECUERDA: el tama�o es en bytes
	//		&obj->vertexPositions[0], GL_STATIC_DRAW);
	//	glVertexAttribPointer(parentProgram->inPos, 3, GL_FLOAT, GL_FALSE, 0, 0);//3: vectores de 3 coord, false: no normalizes, 0: stride, 0: comienzo buffers
	//	glEnableVertexAttribArray(parentProgram->inPos);
	//}
	//if (parentProgram->inColor != -1)
	//{
	//	glGenBuffers(1, &colorVBO);
	//	glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
	//	glBufferData(GL_ARRAY_BUFFER, obj->vertexColor.second * sizeof(float),
	//		obj->vertexColor.first, GL_STATIC_DRAW);
	//	glVertexAttribPointer(parentProgram->inColor, 3, GL_FLOAT, GL_FALSE, 0, 0);
	//	glEnableVertexAttribArray(parentProgram->inColor);
	//}
	//if (parentProgram->inNormal != -1)
	//{
	//	glGenBuffers(1, &normalVBO);
	//	glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
	//	glBufferData(GL_ARRAY_BUFFER, obj->vertexNormals.second * sizeof(float),
	//		obj->vertexNormals.first, GL_STATIC_DRAW);
	//	glVertexAttribPointer(parentProgram->inNormal, 3, GL_FLOAT, GL_FALSE, 0, 0);
	//	glEnableVertexAttribArray(parentProgram->inNormal);
	//}
	//if (parentProgram->inTexCoord != -1)
	//{
	//	glGenBuffers(1, &texCoordVBO);
	//	glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
	//	glBufferData(GL_ARRAY_BUFFER, obj->vertexUVs.second * sizeof(float),
	//		obj->vertexUVs.first, GL_STATIC_DRAW);
	//	glVertexAttribPointer(parentProgram->inTexCoord, 2, GL_FLOAT, GL_FALSE, 0, 0);
	//	glEnableVertexAttribArray(parentProgram->inTexCoord);
	//}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		obj->meshInfo.triangles.sizeOfArray * sizeof(unsigned int), obj->meshInfo.triangles.arrayData ,
		GL_STATIC_DRAW);


	


	//Future todo: check which objs have the same texture to share its identifier
	//IN THE CASE that LoadText is not smart enough to check if a texture has
	//already been loaded
	texturesIDs.colorTexId = LoadTex(obj->texturePaths.colorTexPath);
	texturesIDs.normalTexId = LoadTex(obj->texturePaths.normalTexPath);
	texturesIDs.specTexId = LoadTex(obj->texturePaths.specTexPath);
	texturesIDs.emiTexId = LoadTex(obj->texturePaths.emiTexPath);
	texturesIDs.displTexID = LoadTex(obj->texturePaths.displTexPath);
}

void RendererObject::Render(Camera *cam, OGLProgram *parentProgram)
{
	glm::mat4 view = cam->GetView();
	glm::mat4 model = obj->GetModelMatrix();
	glm::mat4 proj = cam->GetProjection();

	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));

	//Si alguna de estas variables no se usa, no se sube
	if (parentProgram->uModelViewMat != -1)
		glUniformMatrix4fv(parentProgram->uModelViewMat, 1, GL_FALSE,//Se sube como una unica matriz y NO se transpone por el false
			&(modelView[0][0]));

	if (parentProgram->uModelViewProjMat != -1)
		glUniformMatrix4fv(parentProgram->uModelViewProjMat, 1, GL_FALSE,
			&(modelViewProj[0][0]));

	if (parentProgram->uNormalMat != -1)
		glUniformMatrix4fv(parentProgram->uNormalMat, 1, GL_FALSE,
			&(normal[0][0]));

	if (parentProgram->uProjectionMat != -1)
		glUniformMatrix4fv(parentProgram->uProjectionMat, 1, GL_FALSE,
			&(proj[0][0]));
	
	
	

	//Texturas <-- Esto solo hace falta hacerlo una vez CADA VEZ QUE SE CAMBIA DE TEXTURA

	BindTexture(parentProgram->uColorTex, texturesIDs.colorTexId, 0);
	BindTexture(parentProgram->uEmiTex, texturesIDs.emiTexId, 1);
	BindTexture(parentProgram->uSpecTex, texturesIDs.specTexId, 2);
	BindTexture(parentProgram->uNormalTex, texturesIDs.normalTexId, 3);
	BindTexture(parentProgram->uDisplTex, texturesIDs.displTexID, 4);

	//if (uDiffuseIntensity != -1) //TODO: nos hemos quedado intentando pasarle la variable a GPU
	//	glUniform3fv(uDiffuseIntensity, 1, &(diffuseIntensity[0]));
	glBindVertexArray(vao);
	if (!obj->shaderPaths.tessCtrlShader.empty())
		glPatchParameteri(GL_PATCH_VERTICES, obj->dimensionPatch);

	//Future todo: for now we are always using Triangle List drawing
	glDrawElements(obj->drawType, obj->meshInfo.triangles.sizeOfArray,
		GL_UNSIGNED_INT, (void*)0);//ESTO es el RENDER CALL
}

int RendererObject::LoadTex(string fileName)
{
	if (fileName.empty()) return -1;

	unsigned char *map;
	unsigned int w, h;

	map = loadTexture(fileName.c_str(), w, h);
	if (!map)
	{
		std::cout << "Error cargando el fichero: "
			<< fileName.c_str() << std::endl;
		exit(-1);
	}

	unsigned int texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, (GLvoid*)map); //Reserva espacio para la textura2d activa y la sube. Es mutable.

	delete[] map;

	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);

	//https://stackoverflow.com/questions/18447881/opengl-anisotropic-filtering-support-contradictory-check-results
	//if (glewIsSupported("GL_EXT_texture_filter_anisotropic"))
	//{
		GLfloat fLargest;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);
	//}

	return texId;
}

void RendererObject::destroy()
{
	if (parentProgram->inPos != -1) glDeleteBuffers(1, &posVBO);
	if (parentProgram->inColor != -1) glDeleteBuffers(1, &colorVBO);
	if (parentProgram->inNormal != -1) glDeleteBuffers(1, &normalVBO);
	if (parentProgram->inTexCoord != -1) glDeleteBuffers(1, &texCoordVBO);
	glDeleteBuffers(1, &triangleIndexVBO);
	glDeleteVertexArrays(1, &vao);

	glDeleteTextures(1, &texturesIDs.colorTexId);
	glDeleteTextures(1, &texturesIDs.normalTexId);
	glDeleteTextures(1, &texturesIDs.specTexId);
	glDeleteTextures(1, &texturesIDs.emiTexId);
}