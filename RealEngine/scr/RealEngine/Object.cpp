#include "Object.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h> 
#include <vector>
#include <functional>
#include <assimp/postprocess.h>

//Object::Object(cArrayAndSizeI triangleIndex, cArrayAndSizeF cubeVertexPos, cArrayAndSizeF cubeVertexNormal,
//	cArrayAndSizeF cubeVertexColor, cArrayAndSizeF cubeVertexTexCoord, cArrayAndSizeF cubeVertexTangent,
//	string colorTexPath, string normalTexPath, string specTexPath, string emiTexPath,
//	string vertShader, string fragShader, 
//	vec3 position, vec3 rotation, vec3 scale) : Placeable(position, rotation, scale)
//{
//	//modelMatrix = glm::mat4(1.0);
//	triangles = triangleIndex;
//	vertexPositions = cubeVertexPos;
//	vertexColor = cubeVertexColor;
//	vertexNormals = cubeVertexNormal;
//	vertexUVs = cubeVertexTexCoord;
//	vertexTangents = cubeVertexTangent;
//
//	this->colorTexPath = colorTexPath;
//	this->normalTexPath = normalTexPath;
//	this->specTexPath = specTexPath;
//	this->emiTexPath = emiTexPath;
//
//	this->vertShader = vertShader;
//	this->fragShader = fragShader;
//}


Object::Object(string meshPath, ShaderPaths shaderPaths, TexturePaths texturePaths,
	GLenum drawType, GLint dimensionPatch,
	vec3 position, vec3 rotation, vec3 scale)
	:Object(shaderPaths, texturePaths, drawType, dimensionPatch, position, rotation, scale)
{
	meshInfo = LoadMesh(meshPath);
}

Object::Object
(
	MeshInfo meshInfo, ShaderPaths shaderPaths, TexturePaths texturePaths,
	GLenum drawType, GLint dimensionPatch,
	vec3 position, vec3 rotation, vec3 scale)
	: Object(shaderPaths, texturePaths, drawType, dimensionPatch, position, rotation, scale)
{
	this->meshInfo = meshInfo;
}

Object::Object(ShaderPaths shaderPaths, TexturePaths texturePaths,
	GLenum drawType, GLint dimensionPatch,
	vec3 position, vec3 rotation, vec3 scale
) : Placeable(position, rotation, scale)
{
	this->shaderPaths = shaderPaths;
	this->texturePaths = texturePaths;
	this->drawType = drawType;
	this->dimensionPatch = dimensionPatch;
}

/*
Object::Object(cArrayAndSizeI triangleIndex, cArrayAndSizeF cubeVertexPos, cArrayAndSizeF cubeVertexNormal,
	string colorTexPath, string normalTexPath, string specTexPath, string emiTexPath,
	string vertShader, string fragShader,
	GLenum drawType, GLint dimensionPatch,
	vec3 position, vec3 rotation, vec3 scale
) :Placeable(position, rotation, scale)
{
	triangles = triangleIndex;
	vertexPositions = cubeVertexPos;
	vertexNormals = cubeVertexNormal;

	//	vertexColor = cubeVertexColor;
	//	vertexUVs = cubeVertexTexCoord;
	//	vertexTangents = cubeVertexTangent;

	this->colorTexPath = colorTexPath;
	this->normalTexPath = normalTexPath;
	this->specTexPath = specTexPath;
	this->emiTexPath = emiTexPath;

	this->vertShader = vertShader;
	this->fragShader = fragShader;
	this->geomShader = "";
	this->tessCtrlShader = "";
	this->tessEvShader = "";

	this->drawType = drawType;
	this->dimensionPatch = dimensionPatch;
}

Object::Object(cArrayAndSizeI triangleIndex, cArrayAndSizeF cubeVertexPos, cArrayAndSizeF cubeVertexNormal,
	string colorTexPath, string normalTexPath, string specTexPath, string emiTexPath,
	string vertShader, string fragShader, string geomShader, string tessCtrlShader, string tessEvShader,
	GLenum drawType, GLint dimensionPatch,
	vec3 position, vec3 rotation, vec3 scale
	) :Placeable(position, rotation, scale)
{
	triangles = triangleIndex;
	vertexPositions = cubeVertexPos;
	vertexNormals = cubeVertexNormal;

	//	vertexColor = cubeVertexColor;
	//	vertexUVs = cubeVertexTexCoord;
	//	vertexTangents = cubeVertexTangent;

	this->colorTexPath = colorTexPath;
	this->normalTexPath = normalTexPath;
	this->specTexPath = specTexPath;
	this->emiTexPath = emiTexPath;

	this->vertShader = vertShader;
	this->fragShader = fragShader;
	this->geomShader = geomShader;
	this->tessCtrlShader = tessCtrlShader;
	this->tessEvShader = tessEvShader;

	this->drawType = drawType;
	this->dimensionPatch = dimensionPatch;
}

Object::Object(string meshPath, string colorTexPath, string normalTexPath,
	string specTexPath, string emiTexPath,
	string vertShader, string fragShader, string geomShader, string tessCtrlShader, string tessEvShader,
	GLenum drawType, GLint dimensionPatch, vec3 position, vec3 rotation, vec3 scale)
	:Placeable(position, rotation, scale)
{
	LoadMesh(meshPath);

	this->colorTexPath = colorTexPath;
	this->normalTexPath = normalTexPath;
	this->specTexPath = specTexPath;
	this->emiTexPath = emiTexPath;

	this->vertShader = vertShader;
	this->fragShader = fragShader;
	this->geomShader = geomShader;
	this->tessCtrlShader = tessCtrlShader;
	this->tessEvShader = tessEvShader;

	this->drawType = drawType;
	this->dimensionPatch = dimensionPatch;
}

Object::Object(string meshPath, string colorTexPath, string normalTexPath,
	string specTexPath, string emiTexPath,
	string vertShader, string fragShader, string tessCtrlShader, string tessEvShader,
	GLenum drawType, GLint dimensionPatch, vec3 position, vec3 rotation, vec3 scale)
	:Placeable(position, rotation, scale)
{
	LoadMesh(meshPath);

	this->colorTexPath = colorTexPath;
	this->normalTexPath = normalTexPath;
	this->specTexPath = specTexPath;
	this->emiTexPath = emiTexPath;

	this->vertShader = vertShader;
	this->fragShader = fragShader;
	this->geomShader = "";
	this->tessCtrlShader = tessCtrlShader;
	this->tessEvShader = tessEvShader;

	this->drawType = drawType;
	this->dimensionPatch = dimensionPatch;
}

Object::Object(string meshPath, string colorTexPath, string normalTexPath,
	string specTexPath, string emiTexPath,
	string vertShader, string fragShader, string geomShader, GLenum drawType,
	vec3 position, vec3 rotation, vec3 scale)
	:Placeable(position, rotation, scale)
{
	LoadMesh(meshPath);

	this->colorTexPath = colorTexPath;
	this->normalTexPath = normalTexPath;
	this->specTexPath = specTexPath;
	this->emiTexPath = emiTexPath;

	this->vertShader = vertShader;
	this->fragShader = fragShader;
	this->geomShader = geomShader;
	this->tessCtrlShader = "";
	this->tessEvShader = "";

	this->drawType = drawType;
}

Object::Object(string meshPath, string colorTexPath, string normalTexPath,
	string specTexPath, string emiTexPath,
	string vertShader, string fragShader, GLenum drawType,
	vec3 position, vec3 rotation, vec3 scale)
	:Placeable(position, rotation, scale)
{
	LoadMesh(meshPath);

	this->colorTexPath = colorTexPath;
	this->normalTexPath = normalTexPath;
	this->specTexPath = specTexPath;
	this->emiTexPath = emiTexPath;

	this->vertShader = vertShader;
	this->fragShader = fragShader;
	this->geomShader = "";
	this->tessCtrlShader = "";
	this->tessEvShader = "";

	this->drawType = drawType;
}

*/
void LoadAssimpArrayIntoVector(const aiVector3D *array,
	const unsigned int sizeArray, std::vector<float> &output)
{
	output.reserve(sizeArray * 3);
	for (size_t i = 0; i < sizeArray; i++)
	{
		aiVector3D assimpVector = array[i];

		output.push_back(assimpVector.x);
		output.push_back(assimpVector.y);
		output.push_back(assimpVector.z);
	}
}

void LoadAssimpArrayIntoVector(const aiColor4D *array,
	const unsigned int sizeArray, std::vector<float> &output)
{
	output.reserve(sizeArray * 4);
	for (size_t i = 0; i < sizeArray; i++)
	{
		aiColor4D assimpVector = array[i];

		output.push_back(assimpVector.r);
		output.push_back(assimpVector.g);
		output.push_back(assimpVector.b);
		output.push_back(assimpVector.a);
	}
}

void LoadAssimpArrayIntoVector(const aiFace *array,
	const unsigned int sizeArray, std::vector<unsigned int> &output)
{
	output.reserve(sizeArray * 3);
	for (size_t i = 0; i < sizeArray; i++)
	{
		aiFace assimpVector = array[i];

		//Supposing they are all triangles...
		output.push_back(assimpVector.mIndices[0]);
		output.push_back(assimpVector.mIndices[1]);
		output.push_back(assimpVector.mIndices[2]);
	}
}

void LoadUVArrayIntoVector(const aiVector3D *array,
	const unsigned int sizeArray, std::vector<float> &output)
{
	output.reserve(sizeArray * 2);

	for (size_t i = 0; i < sizeArray; i++)
	{
		aiVector3D assimpVector = array[i];

		output.push_back(assimpVector.x);
		output.push_back(assimpVector.y);
	}
}


template<class inputClass, class vectorClass>
std::vector<vectorClass> DumpIntoIGlib(bool condition, const inputClass* input, const unsigned int size,
	std::function<void(const inputClass*, const unsigned int, std::vector<vectorClass>&)> loadFunc)
{
	std::vector<vectorClass> vec(0);
	if (condition)
	{
		loadFunc(input, size, vec);
	}

	return vec;
}

Object::MeshInfo Object::LoadMesh(string path)
{
	MeshInfo loadedMesh;

	//ASSIMP
	// Create an instance of the Importer class
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path,
		aiProcess_CalcTangentSpace |
		aiProcess_GenNormals |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType |
		aiProcess_GenUVCoords |
		aiProcess_TransformUVCoords);

	// If the import failed, report it
	if (scene == nullptr) throw("Scene could not be loaded.");
	// Now we can access the file's contents. 

	unsigned int * listindex = new unsigned int[scene->mMeshes[0]->mNumFaces * 3];
	for (int i = 0; i < scene->mMeshes[0]->mNumFaces; i++)
	{
		aiFace temptriangle = scene->mMeshes[0]->mFaces[i];
		listindex[i * 3 + 0] = temptriangle.mIndices[0];
		listindex[i * 3 + 1] = temptriangle.mIndices[1];
		listindex[i * 3 + 2] = temptriangle.mIndices[2];
	}

	float * listpos = new float[scene->mMeshes[0]->mNumVertices * 3];
	for (int i = 0; i < scene->mMeshes[0]->mNumVertices; i++)
	{
		listpos[i * 3 + 0] = scene->mMeshes[0]->mVertices[i].x;
		listpos[i * 3 + 1] = scene->mMeshes[0]->mVertices[i].y;
		listpos[i * 3 + 2] = scene->mMeshes[0]->mVertices[i].z;
	}

	float * listnorm = new float[scene->mMeshes[0]->mNumVertices * 3];
	for (int i = 0; i < scene->mMeshes[0]->mNumVertices; i++)
	{
		listnorm[i * 3 + 0] = scene->mMeshes[0]->mNormals[i].x;
		listnorm[i * 3 + 1] = scene->mMeshes[0]->mNormals[i].y;
		listnorm[i * 3 + 2] = scene->mMeshes[0]->mNormals[i].z;
	}

	float * listtextcoord = NULL;
	if (scene->mMeshes[0]->HasTextureCoords(0))
	{
		listtextcoord = new float[scene->mMeshes[0]->mNumVertices * 2]();
		for (int i = 0; i < scene->mMeshes[0]->mNumVertices; i++)
		{
			listtextcoord[i * 2 + 0] = scene->mMeshes[0]->mTextureCoords[0][i].x;
			listtextcoord[i * 2 + 1] = scene->mMeshes[0]->mTextureCoords[0][i].y;
		}
	}

	float * listcol = new float[scene->mMeshes[0]->mNumVertices * 3];
	if (scene->mMeshes[0]->mColors[0] == NULL)
		for (int i = 0; i < scene->mMeshes[0]->mNumVertices; i++)
		{
			listcol[i * 3 + 0] = scene->mMeshes[0]->mNormals[i].x;
			listcol[i * 3 + 1] = scene->mMeshes[0]->mNormals[i].y;
			listcol[i * 3 + 2] = scene->mMeshes[0]->mNormals[i].z;
		}
	else
		for (int i = 0; i < scene->mMeshes[0]->mNumVertices; i++)
		{
			listcol[i * 3 + 0] = scene->mMeshes[0]->mColors[i]->r;
			listcol[i * 3 + 1] = scene->mMeshes[0]->mColors[i]->g;
			listcol[i * 3 + 2] = scene->mMeshes[0]->mColors[i]->b;
			//listcol[i * 4 + 3] = 0.0f;
		}

	
	float * listtangent = NULL;
	if (scene->mMeshes[0]->HasTangentsAndBitangents())
	{
		listtangent = new float[scene->mMeshes[0]->mNumVertices * 3];
		for (int i = 0; i < scene->mMeshes[0]->mNumVertices; i++)
		{
			listtangent[i * 3 + 0] = scene->mMeshes[0]->mTangents[i].x;
			listtangent[i * 3 + 1] = scene->mMeshes[0]->mTangents[i].y;
			listtangent[i * 3 + 2] = scene->mMeshes[0]->mTangents[i].z;
		}
	}
	//END ASSIMP

	loadedMesh.vertexPositions.arrayData = listpos;
	loadedMesh.vertexPositions.sizeOfArray = scene->mMeshes[0]->mNumVertices * 3;
	loadedMesh.vertexColor.arrayData = listcol;
	loadedMesh.vertexColor.sizeOfArray = scene->mMeshes[0]->mNumVertices * 3;
	loadedMesh.vertexNormals.arrayData = listnorm;
	loadedMesh.vertexNormals.sizeOfArray = scene->mMeshes[0]->mNumVertices * 3;
	loadedMesh.vertexUVs.arrayData = listtextcoord;
	loadedMesh.vertexUVs.sizeOfArray = scene->mMeshes[0]->mNumVertices * 2;
	loadedMesh.vertexTangents.arrayData = listtangent;
	loadedMesh.vertexTangents.sizeOfArray = scene->mMeshes[0]->mNumVertices * 3;
	loadedMesh.triangles.arrayData = listindex;
	loadedMesh.triangles.sizeOfArray = scene->mMeshes[0]->mNumFaces * 3;

	return loadedMesh;


	//Assimp::Importer importer;
	//const aiScene *scene = importer.ReadFile(path, 
	//	aiProcess_CalcTangentSpace |
	//	aiProcess_GenNormals |
	//	aiProcess_Triangulate |
	//	aiProcess_JoinIdenticalVertices |
	//	aiProcess_SortByPType |
	//	aiProcess_GenUVCoords |
	//	aiProcess_TransformUVCoords);

	//if (scene == nullptr) throw("Scene could not be loaded.");
	//aiMesh *importedMesh = scene->mMeshes[0]; //TODO: get more than the first imported mesh of the scene

	//std::function<void(const aiVector3D*, const unsigned int, std::vector<float>&)> vec3DAssLoad =
	//	static_cast<void(*)(const aiVector3D*, const unsigned int, std::vector<float>&)>(LoadAssimpArrayIntoVector);
	//std::function<void(const aiColor4D*, const unsigned int, std::vector<float>&)> col4DAssLoad =
	//	static_cast<void(*)(const aiColor4D*, const unsigned int, std::vector<float>&)>(LoadAssimpArrayIntoVector);
	//std::function<void(const aiFace*, const unsigned int, std::vector<unsigned int>&)> faceAssLoad =
	//	static_cast<void(*)(const aiFace*, const unsigned int, std::vector<unsigned int>&)>(LoadAssimpArrayIntoVector);

	//std::vector<float> verts = DumpIntoIGlib<aiVector3D, float>(true, importedMesh->mVertices, importedMesh->mNumVertices, vec3DAssLoad);
	//std::vector<float> vertColors = DumpIntoIGlib<aiColor4D, float>(importedMesh->HasVertexColors(0), importedMesh->mColors[0], importedMesh->mNumVertices, col4DAssLoad);
	//std::vector<float> normals = DumpIntoIGlib<aiVector3D, float>(importedMesh->HasNormals(), importedMesh->mNormals, importedMesh->mNumVertices, vec3DAssLoad);
	//std::vector<float> texCoords = DumpIntoIGlib<aiVector3D, float>(importedMesh->HasTextureCoords(0), importedMesh->mTextureCoords[0], importedMesh->mNumVertices, LoadUVArrayIntoVector);
	//std::vector<float> tangents = DumpIntoIGlib<aiVector3D, float>(importedMesh->HasTangentsAndBitangents(), importedMesh->mTangents, importedMesh->mNumVertices, vec3DAssLoad);
	//std::vector<unsigned int> trianglesV = DumpIntoIGlib<aiFace, unsigned int>(true, importedMesh->mFaces, importedMesh->mNumFaces, faceAssLoad);

	//auto firstValueAsPointer = [](std::vector<float> &v) -> float* { return (v.empty() ? NULL : &v[0]); };

	//versin de almacenamiento que est activa
	//vertexPositions.arrayData = firstValueAsPointer(verts);
	//vertexPositions.sizeOfArray = verts.size();

	//vertexColor.arrayData = firstValueAsPointer(vertColors);
	//vertexColor.sizeOfArray = vertColors.size();

	//vertexNormals.arrayData = firstValueAsPointer(normals);
	//vertexNormals.sizeOfArray = normals.size();

	//vertexUVs.arrayData = firstValueAsPointer(texCoords);
	//vertexUVs.sizeOfArray = texCoords.size();

	//vertexTangents.arrayData = firstValueAsPointer(tangents);
	//vertexTangents.sizeOfArray = tangents.size();

	//triangles.arrayData = &trianglesV[0];
	//triangles.sizeOfArray = trianglesV.size();

	//versin de almacenamiento 2
	//vertexPositions = cArrayAndSizeF(firstValueAsPointer(verts), verts.size());
	//vertexColor = cArrayAndSizeF(firstValueAsPointer(vertColors), vertColors.size());
	//vertexNormals = cArrayAndSizeF(firstValueAsPointer(normals), normals.size());
	//vertexUVs = cArrayAndSizeF(firstValueAsPointer(texCoords), texCoords.size());
	//vertexTangents = cArrayAndSizeF(firstValueAsPointer(tangents), tangents.size());
	//triangles = cArrayAndSizeI(&trianglesV[0], trianglesV.size());

	//versin de almacenamiento 3
	//vertexPositions = verts;
	//vertexColor = vertColors;
	//vertexNormals = normals;
	//vertexUVs = texCoords;
	//vertexTangents = tangents;
	//triangles = trianglesV;
}
