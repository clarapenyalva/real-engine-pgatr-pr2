#include "Camera.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

glm::vec3 right(1.0f, 0.0f, 0.0f),
up(0.0f, 1.0f, 0.0f),
forward(0.0f, 0.0f, -1.0f);

Camera::Camera(vec3 position, vec3 forward, vec3 up,
	float nearPlane, float farPlane, float aspectRatio, float fovDegrees)
{
	SetCamera(position, forward, up);
	SetProjection(nearPlane, farPlane, aspectRatio, fovDegrees);
}

/*Camera::Camera(vec3 position, vec3 rotations,
	float nearPlane, float farPlane, float aspectRatio, float fovDegrees)
{
	SetCamera(position, rotations);
	SetProjection(nearPlane, farPlane, aspectRatio, fovDegrees);
}*/

Camera::Camera()
{
	//SetCamera(vec3(0), vec3(0, 0, -1), vec3(0, 1, 0));
	//SetProjection(0.1f, 10, 1, 45);
}

mat4 Camera::GetView()
{
	return viewMatrix;
}

void Camera::SetView(mat4 newView) { viewMatrix = newView; }


mat4 Camera::GetProjection()
{
	return projectionMatrix;
}

void Camera::SetCamera(vec3 position, vec3 forward, vec3 up)
{
	this->position = position;
	up = normalize(up);
	forward = normalize(forward);
	this->up = up;
	this->forward = forward;


	//viewMatrix = lookAt(position, position + forward, up);
	viewMatrix = lookAt(position, forward + position, up);
	//Faltan definir l�s �ngulos en base a forward y up
}

//void Camera::SetCamera(vec3 position, vec3 rotations)
//{
//	this->position = position;
//	this->rotations = rotations;
//
//
//	mat4 modelMatrix(1);
//
//	modelMatrix = mat4(1.0f);
//	modelMatrix = glm::translate(modelMatrix, position);
//	modelMatrix = glm::rotate(modelMatrix, rotations.x, glm::vec3(1.0f, 0.0f, 0.0f));
//	modelMatrix = glm::rotate(modelMatrix, rotations.y, glm::vec3(0.0f, 1.0f, 0.0f));
//	modelMatrix = glm::rotate(modelMatrix, rotations.z, glm::vec3(0.0f, 0.0f, 1.0f));
//
//	viewMatrix = glm::inverse(modelMatrix);
//
//	//this->up = vec3(viewMatrix * vec4(up, 0));
//	//this->forward = vec3(viewMatrix * vec4(forward, 0));
//}

void Camera::SetPosition(vec3 position)
{
	//Placeable::SetPosition(position);
	SetCamera(position, forward, up);
}

vec3 Camera::GetPosition()
{
	return position;
}

/*vec3 Camera::GetRotation()
{
	return rotations;
}

void Camera::SetRotation(vec3 rotations)
{
	this->rotations = rotations;
	SetCamera(position, rotations);
}*/

vec3 Camera::GetForward() 
{
	return this->forward;
}

vec3 Camera::GetRight()
{
	return cross(this->forward, this->up);
}

void Camera::SetForward(vec3 forward)
{
	this->forward = normalize(forward);
	SetCamera(position, forward, up);
}

void Camera::SetUpVector(vec3 up)
{
	this->up = normalize(up);
	SetCamera(position, forward, up);
}

void Camera::SetProjection(float nearPlane, float farPlane, float aspectRatio, float fovDegrees)
{

	this->nearPlane = nearPlane;
	this->farPlane = farPlane;
	this->aspectRatio = aspectRatio;
	this->fovDegrees = fovDegrees;

	projectionMatrix = mat4(0.0);

	float fovRadians = fovDegrees * PI / 180.0f;

	projectionMatrix[0].x = 1.0f / (aspectRatio * tan(fovRadians / 2.0f));
	projectionMatrix[1].y = 1.0f / tan(fovRadians / 2.0f);
	projectionMatrix[2].z = -(nearPlane + farPlane) / (farPlane - nearPlane);
	projectionMatrix[3].z = -2.0f * farPlane * nearPlane / (farPlane - nearPlane);
	projectionMatrix[2].w = -1.0f;
}

void Camera::SetProjection(mat4 newProjection)
{
	projectionMatrix = newProjection;
}

void Camera::SetFar(float farPlane)
{
	this->farPlane = farPlane;
	SetProjection(nearPlane, farPlane, aspectRatio, fovDegrees);
}

void Camera::SetNear(float nearPlane)
{
	this->nearPlane = nearPlane;
	SetProjection(nearPlane, farPlane, aspectRatio, fovDegrees);
}

void Camera::SetAspectRatio(float aspectRatio)
{
	this->aspectRatio = aspectRatio;
	SetProjection(nearPlane, farPlane, aspectRatio, fovDegrees);
}

void Camera::SetFOV(float fovDegrees)
{
	this->fovDegrees = fovDegrees;
	SetProjection(nearPlane, farPlane, aspectRatio, fovDegrees);
}