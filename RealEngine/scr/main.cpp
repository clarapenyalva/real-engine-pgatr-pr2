﻿#include "BOX.h"
#include "auxiliar.h"
#include "PLANE.h"
#include "RealEngine/RealEngine.h"

#include <windows.h>

#include <gl/glew.h>
#include <gl/gl.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h> 
#include <iostream>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <cstdlib>
#include "QuadBox.h" 

#define RAND_SEED 31415926

//////////////////////////////////////////////////////////////
// Datos que se almacenan en la memoria de la CPU
//////////////////////////////////////////////////////////////

//Matrices
glm::mat4	proj = glm::mat4(1.0f);
glm::mat4	view = glm::mat4(1.0f);
glm::mat4	model = glm::mat4(1.0f);


//////////////////////////////////////////////////////////////
// Variables que nos dan acceso a Objetos OpenGL
//////////////////////////////////////////////////////////////
float angle = 0.0f;



//////////////////////////////////////////////////////////////
// Funciones auxiliares
//////////////////////////////////////////////////////////////

//Declaración de CB
void renderFunc();
void resizeFunc(int width, int height);
void UpdateCameras(Camera &refCamera);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mouseMotionFunc(int x, int y);

//void renderCube();

//Funciones de inicialización y destrucción
void initContext(int argc, char** argv);
void initOGL();
//void initShaderFw(const char *vname, const char *fname);
//void initShaderPP(const char *vname, const char *fname);
//void initObj();
//void initPlane();
void destroy();
//void initFBO();
//void resizeFBO(unsigned int w, unsigned int h);

//////////////////////////////////////////////////////////////
// Nuevas variables auxiliares
//////////////////////////////////////////////////////////////
PostProcess postProcess;
vector<pair<Scene, bool>> scenes;
Scene scene;
Scene dotsScene;
Scene linesScene;
Scene linesCenterScene;
Scene wireFrameScene;
Scene tessSceneTris;
Scene tessSceneQuads;
Scene iniGeomScene;
Scene displSceneA;
Scene displSceneB;
Scene lodDisplScene;
Scene lodDisplSceneLines;

Camera cam;
vector<Object> objs;
vector<Object> dotsObjs;
vector<Object> linesObjs;
vector<Object> linesCenterObjs;
vector<Object> wireFrameObjs;
vector<Object> tessObjsTris;
vector<Object> tessObjsQuads;
vector<Object> iniGeomObjs;
vector<Object> displObjsA;
vector<Object> displObjsB;
vector<Object> lodDisplObjs;
vector<Object> lodDisplObjsLines;

Light dotLight;
DirectionalLight directionalLight;
SpotLight spotLight;

const vec3 ambientalLight(0.2);
const float nearPlane = 1.0f, farPlane = 30.0f;
float fovDegrees = 60.0f;

vec3 cameraPos(0, 0, 5);

vec3 modelPos(0, 0, 0);
float modelScale = 1;

//vector<bool> doDepth;

const vec2 windowSize(500, 500);

//////////////////////////////////////////////////////////////
// Nuevas funciones auxiliares
//////////////////////////////////////////////////////////////
void createAllScenes();
void CreatePassTroughScene(Object &obj, bool render);
void CreateWireFrameScene(Object &obj, bool render);
void CreateNormalsScene(Object &obj, bool render);
void CreateNormalsCenterScene(Object &obj, bool render);
void CreateTrisTessScene(Object &obj, Object::TexturePaths tPaths, bool render);
void CreateDotsScene(Object &obj, bool render);
void CreateQuadsTessScene(Object &obj, bool render);
void CreateDisplScene(Object &obj, bool renderA, bool renderB);
void RenderDisplSceneA(Object & obj, Object::TexturePaths &tPaths, bool &renderA);
void RenderDisplSceneB(Object & obj, Object::TexturePaths &tPaths, bool &renderB);
void CreateLODDisplScenes(Object &obj, bool render);

int main(int argc, char** argv)
{
	std::locale::global(std::locale("spanish"));// acentos ;)

	initContext(argc, argv);
	initOGL();
	//initShaderFw("../shaders/fwRendering.v2.vert", "../shaders/fwRendering.v2.frag");
	//postProcess = PostProcess(windowSize.x, windowSize.y);
	//postProcess.nearPlane = nearPlane;
	//postProcess.farPlane = farPlane;

	createAllScenes();

	glutMainLoop();

	destroy();

	return 0;
}

//////////////////////////////////////////
// Funciones auxiliares 
void initContext(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	//glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(windowSize.x, windowSize.y);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Prácticas GLSL");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit(-1);
	}

	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;

	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(mouseMotionFunc);
}

void initOGL()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.8f, 0.8f, 0.8f, 0.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	glPointSize(5);

	//proj = glm::perspective(glm::radians(60.0f), 1.0f, 1.0f, 50.0f);
	//view = glm::mat4(1.0f);
	//view[3].z = -25.0f;
}

void destroy()
{
	//postProcess.destroy();
	scene.destroy();
}

void createAllScenes()
{
	//Lights
	directionalLight = DirectionalLight(vec3(0.7, 0.2, 0.2), vec3(-1, 0, 0));
	spotLight = SpotLight(vec3(0.5, 0.9, 0.5), vec3(1, 0, 0), vec3(0, 0, -1), 20, vec3(0, 0, 6));
	// Camera
	cam = Camera(cameraPos, vec3(0, 0, -1), vec3(0, 1, 0), nearPlane, farPlane, windowSize.x / windowSize.y, fovDegrees);

	//Objects!

	Object::ShaderPaths shaderPaths(
		"shaders/fwRendering.v2.vert",
		"shaders/fwRendering.v2.frag",
		"", "", "");

	Object::TexturePaths texPaths
	(
		"img/color2.png",
		"img/normal.png",
		"img/specMap.png",
		"img/emissive.png",
		""
	);

	Object obj = Object("./models/bunnyLP.obj", shaderPaths, texPaths, GL_TRIANGLES, 3,
		modelPos, vec3(0, 0, 0), vec3(modelScale));

	objs.push_back(obj);
	scene = Scene(objs, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(scene, true)); //0
	//doDepth.push_back(true);

	//Ex1.1
	CreatePassTroughScene(obj, false); //1
	CreateDotsScene(obj, false); //2
	CreateNormalsScene(obj, false); //3
	CreateNormalsCenterScene(obj, false); //4
	CreateWireFrameScene(obj, false); //5

	//Ex1.2
	CreateTrisTessScene(obj, texPaths, false); //6
	CreateQuadsTessScene(obj, false); //7

	//Ex2
	CreateDisplScene(obj, false, false); //8-9
	CreateLODDisplScenes(obj, false); //10-11


}

void CreatePassTroughScene(Object &obj, bool render)
{
	Object iniGeomObject(obj);
	iniGeomObject.shaderPaths.vertShader = "shaders/InitialGeom.vert";
	iniGeomObject.shaderPaths.fragShader = "shaders/InitialGeom.frag";
	iniGeomObject.shaderPaths.geomShader = "shaders/InitialGeom.geom";
	iniGeomObject.drawType = GL_TRIANGLES;
	iniGeomObjs.push_back(iniGeomObject);
	iniGeomScene = Scene(iniGeomObjs, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(iniGeomScene, render));
	//doDepth.push_back(true);

}

void CreateLODDisplScenes(Object &obj, bool render)
{
	Object::ShaderPaths tessShaderPaths(
		"shaders/LODDisplVertShader.glsl",
		"shaders/LODDisplFragShader.glsl",
		"shaders/LODDisplGeomShader.glsl",
		"shaders/LODDisplCtrlShader.glsl",
		"shaders/LODDisplEvShader.glsl");

	Object::TexturePaths tPaths
	(
		/*"img/tiles/tiles_color.png",
		"",
		"img/tiles/tiles_met.png",
		"",
		"img/tiles/tiles_height.png"*/

		"img/bricks/bricks_color.png",
		"",
		"",
		"",
		"img/bricks/bricks_height.png"
	);

	Object lodDisplObject(obj.meshInfo, tessShaderPaths, tPaths,
		GL_PATCHES, 3,
		modelPos, vec3(0, 0, 0), vec3(modelScale));



	lodDisplObjs.push_back(lodDisplObject);

	lodDisplScene = Scene(lodDisplObjs, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(lodDisplScene, render));
	//doDepth.push_back(true);

	//Wireframes scene
	Object::ShaderPaths tessShaderPathsLines(
		"shaders/LODDisplVertShader.glsl",
		"shaders/LODDisplFragShaderLines.glsl",
		"shaders/LODDisplGeomShaderLines.glsl",
		"shaders/LODDisplCtrlShader.glsl",
		"shaders/LODDisplEvShader.glsl");

	Object lodDisplObjectLines(obj.meshInfo, tessShaderPathsLines, tPaths,
		GL_PATCHES, 3,
		modelPos, vec3(0, 0, 0), vec3(modelScale));

	lodDisplObjsLines.push_back(lodDisplObjectLines);

	lodDisplSceneLines = Scene(lodDisplObjsLines, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(lodDisplSceneLines, false));
	//doDepth.push_back(false);




}

void CreateDisplScene(Object &obj, bool renderA, bool renderB)
{
	Object::TexturePaths tPaths
	(
		"img/earth2.jpg", "", "", "", "img/earth2.jpg"
	);

	RenderDisplSceneA(obj, tPaths, renderA);

	RenderDisplSceneB(obj, tPaths, renderB);
}

void RenderDisplSceneA(Object & obj, Object::TexturePaths &tPaths, bool &renderA)
{
	Object::ShaderPaths tessShaderPathsA(
		"shaders/DisplVertShader.glsl",
		"shaders/DisplFragShader.glsl",
		"shaders/DisplGeomShaderA.glsl",
		"shaders/DisplCtrlShader.glsl",
		"shaders/DisplEvShaderA.glsl");



	Object displObjectA(obj.meshInfo, tessShaderPathsA, tPaths,
		GL_PATCHES, 3,
		vec3(0), vec3(0, 0, 0), vec3(modelScale));

	displObjsA.push_back(displObjectA);

	displSceneA = Scene(displObjsA, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(displSceneA, renderA));
	//doDepth.push_back(true);

}

void RenderDisplSceneB(Object & obj, Object::TexturePaths &tPaths, bool &renderB)
{
	Object::ShaderPaths tessShaderPathsB(
		"shaders/DisplVertShader.glsl",
		"shaders/DisplFragShader.glsl",
		"shaders/DisplGeomShaderB.glsl",
		"shaders/DisplCtrlShader.glsl",
		"shaders/DisplEvShaderB.glsl");

	Object displObjectB(obj.meshInfo, tessShaderPathsB, tPaths,
		GL_PATCHES, 3,
		vec3(0), vec3(0, 0, 0), vec3(modelScale));

	displObjsB.push_back(displObjectB);

	displSceneB = Scene(displObjsB, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(displSceneB, renderB));
	//doDepth.push_back(true);

}

void CreateWireFrameScene(Object &obj, bool render)
{
	Object::ShaderPaths wfShaderPaths(
		"shaders/WireFrameVertShader.glsl",
		"shaders/WireFrameFragShader.glsl",
		"shaders/WireFrameGeomShader.glsl",
		"",
		"");

	Object wireFrameObject(obj);
	wireFrameObject.shaderPaths = wfShaderPaths;
	/*wireFrameObject.vertShader = "shaders/WireFrameVertShader.glsl";
	wireFrameObject.fragShader = "shaders/WireFrameFragShader.glsl";
	wireFrameObject.geomShader = "shaders/WireFrameGeomShader.glsl";*/
	wireFrameObject.drawType = GL_TRIANGLES;
	wireFrameObjs.push_back(wireFrameObject);

	wireFrameScene = Scene(wireFrameObjs, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(wireFrameScene, render));
	//doDepth.push_back(false);

}

void CreateNormalsScene(Object &obj, bool render)
{
	Object::ShaderPaths lineShaderPaths(
		"shaders/LinesVertShader.glsl",
		"shaders/LinesFragShader.glsl",
		"shaders/LinesGeomShader.glsl",
		"",
		"");

	Object lineObject(obj);
	lineObject.shaderPaths = lineShaderPaths;
	lineObject.drawType = GL_POINTS;
	linesObjs.push_back(lineObject);

	linesScene = Scene(linesObjs, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(linesScene, render));
	//doDepth.push_back(false);

}

void CreateNormalsCenterScene(Object &obj, bool render)
{
	Object::ShaderPaths lineCenterShaderPaths(
		"shaders/NormalsCenterVertShader.glsl",
		"shaders/NormalsCenterFragShader.glsl",
		"shaders/NormalsCenterGeomShader.glsl",
		"",
		"");

	Object lineCenterObject(obj);
	lineCenterObject.shaderPaths = lineCenterShaderPaths;
	lineCenterObject.drawType = GL_TRIANGLES;
	linesCenterObjs.push_back(lineCenterObject);

	linesCenterScene = Scene(linesCenterObjs, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(linesCenterScene, render));
	//doDepth.push_back(false);

}

void CreateTrisTessScene(Object &obj, Object::TexturePaths tPaths, bool render)
{
	Object::ShaderPaths tessShaderPaths(
		"shaders/TessVertShader.glsl",
		"shaders/TessFragShader.glsl",
		"shaders/TessGeomShaderTris.glsl",
		"shaders/TessTessCtrlShaderTris.glsl",
		"shaders/TessTessEvShaderTris.glsl");

	Object tessObjectTris(obj.meshInfo, tessShaderPaths, tPaths,
		GL_PATCHES, 3,
		vec3(0), vec3(0, 0, 0), vec3(modelScale));
	tessObjectTris.shaderPaths = tessShaderPaths;
	/*tessObjectTris.vertShader = "shaders/TessVertShader.glsl";
	tessObjectTris.fragShader = "shaders/TessFragShader.glsl";
	tessObjectTris.geomShader = "shaders/TessGeomShaderTris.glsl";
	tessObjectTris.tessCtrlShader = "shaders/TessTessCtrlShaderTris.glsl";
	tessObjectTris.tessEvShader = "shaders/TessTessEvShaderTris.glsl";*/
	//tessObjectTris.drawType = GL_PATCHES;
	//tessObjectTris.dimensionPatch = 3;
	tessObjsTris.push_back(tessObjectTris);

	tessSceneTris = Scene(tessObjsTris, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(tessSceneTris, render));
	//doDepth.push_back(true);



}

void CreateDotsScene(Object &obj, bool render)
{
	Object::ShaderPaths dotsShaderPaths(
		"shaders/DotsVertShader.glsl",
		"shaders/DotsFragShader.glsl",
		"shaders/DotsGeomShader.glsl",
		"",
		"");

	Object dotsObject(obj);
	dotsObject.shaderPaths = dotsShaderPaths;
	/*dotsObject.vertShader = "shaders/DotsVertShader.glsl";
	dotsObject.fragShader = "shaders/DotsFragShader.glsl";
	dotsObject.geomShader = "shaders/DotsGeomShader.glsl";*/
	dotsObject.drawType = GL_POINTS;
	dotsObjs.push_back(dotsObject);

	dotsScene = Scene(dotsObjs, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(dotsScene, render));
	//doDepth.push_back(false);

}

void CreateQuadsTessScene(Object &obj, bool render)
{
	cArrayAndSizeI cubeTriangleIndexPair;
	cArrayAndSizeF cubeVertexPosPair;
	cArrayAndSizeF cubeVertexNormalPair;

	cubeTriangleIndexPair.arrayData = &cubeQuadsIndices[0];
	cubeTriangleIndexPair.sizeOfArray = cubeQuadsNumFaces * 4;

	cubeVertexPosPair.arrayData = &cubeQuadsVertices[0];
	cubeVertexPosPair.sizeOfArray = cubeQuadsNumVerts * 3;

	cubeVertexNormalPair.arrayData = &cubeQuadsNormals[0];
	cubeVertexNormalPair.sizeOfArray = cubeQuadsNumVerts * 3;

	Object::MeshInfo meshInfo(
		cubeVertexPosPair,
		cubeVertexNormalPair,
		cubeTriangleIndexPair
	);

	Object::ShaderPaths tessShaderPaths(
		"shaders/TessVertShader.glsl",
		"shaders/TessFragShader.glsl",
		"shaders/TessGeomShaderQuads.glsl",
		"shaders/TessTessCtrlShaderQuads.glsl",
		"shaders/TessTessEvShaderQuads.glsl");

	/*Object tessObjectQuad("./models/quadQUAD.obj",
	tessShaderPaths, obj.texturePaths,
	GL_PATCHES, 4,
	vec3(0), vec3(0, 0, 0), vec3(modelScale));*/

	Object tessObjectQuad(meshInfo,
		tessShaderPaths, obj.texturePaths,
		GL_PATCHES, 4,
		vec3(0), vec3(0, 0, 0), vec3(modelScale));


	tessObjsQuads.push_back(tessObjectQuad);

	tessSceneQuads = Scene(tessObjsQuads, directionalLight, dotLight, spotLight, cam, ambientalLight);
	scenes.push_back(pair<Scene, bool>(tessSceneQuads, render));
	//doDepth.push_back(true);

}

void renderFunc()
{
	//postProcess.PreRender();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //Llamada asíncrona
														//glEnable(GL_DEPTH_TEST);

	for (size_t i = 0; i < scenes.size(); i++)
	{
		if (scenes[i].second)
		{
			/*if (doDepth[i])
			glEnable(GL_DEPTH_TEST);
			else
			glDisable(GL_DEPTH_TEST);*/


			scenes[i].first.RenderScene();

		}
	}

	//scene.RenderScene();
	//dotsScene.RenderScene();
	//linesScene.RenderScene();
	//wireFrameScene.RenderScene();

	//tessSceneTris.RenderScene();
	//tessSceneQuads.RenderScene();
	//displScene.RenderScene();

	//postProcess.Render();

	//postProcess.Render();
	glutSwapBuffers();//Llamada síncrona que además espera al vsync si esta activado
}

void UpdateCameras(Camera &refCamera)
{
	/*dotsScene.camera = scene.camera;
	linesScene.camera = scene.camera;
	wireFrameScene.camera = scene.camera;
	tessSceneTris.camera = scene.camera;
	tessSceneQuads.camera = scene.camera;
	displScene.camera = scene.camera;*/

	for (size_t i = 0; i < scenes.size(); i++)
	{
		scenes[i].first.camera = refCamera;
	}
}

void resizeFunc(int width, int height)
{
	glViewport(0, 0, width, height);
	/*proj = glm::perspective(

	float(width) / float(height),
	1.0f,
	50.0f);*/
	scene.camera.SetProjection(nearPlane, farPlane, (float)width / (float)height, fovDegrees);


	//scene.camera.SetProjection(proj);
	UpdateCameras(scene.camera);
	postProcess.resizeFBO(width, height);

	glutPostRedisplay();
}

void idleFunc()
{
	//angle = (angle > 3.141592f * 2.0f) ? 0 : angle + 0.02f;
	/*angle += 0.2f;
	std::srand(RAND_SEED);
	for (unsigned int i = 1; i < 11; i++)
	{
	float size = float(std::rand() % 3 + 1);

	glm::vec3 axis(glm::vec3(float(std::rand() % 2),
	float(std::rand() % 2), float(std::rand() % 2)));
	if (glm::all(glm::equal(axis, glm::vec3(0.0f))))
	axis = glm::vec3(1.0f);

	float trans = float(std::rand() % 7 + 3) * 1.00f + 0.5f;
	glm::vec3 transVec = axis * trans;
	transVec.x *= (std::rand() % 2) ? 1.0f : -1.0f;
	transVec.y *= (std::rand() % 2) ? 1.0f : -1.0f;
	transVec.z *= (std::rand() % 2) ? 1.0f : -1.0f;

	model = glm::rotate(glm::mat4(1.0f), angle*2.0f*size, axis);
	model = glm::translate(model, transVec);
	model = glm::rotate(model, angle*2.0f*size, axis);
	model = glm::scale(model, glm::vec3(1.0f / (size*0.7f)));
	scene.objects[i].SetModelMatrix(model);
	}*/

	glutPostRedisplay();
}

float radius = 5;
float scrollSpeed = 0.5f;
glm::vec2 prevV(0, 0);
glm::vec2 rotV(0, 0);
glm::vec3 upV(0, 1, 0);
glm::vec3 rightV(1, 0, 0);

void drawOrbitalCamera()
{
	glm::mat4 cameraTransf(1.0);
	cameraTransf = glm::rotate(cameraTransf, -rotV.x * 180.0f/PI, upV);
	cameraTransf = glm::rotate(cameraTransf, -rotV.y* 180.0f / PI, rightV);
	cameraTransf = glm::translate(cameraTransf, glm::vec3(0, 0, radius));

	glm::mat4 view = glm::inverse(cameraTransf);
	scene.camera.SetView(view);
	UpdateCameras(scene.camera);
}

void keyboardFunc(unsigned char key, int x, int y)
{
	//std::cout << "Se ha pulsado la tecla " << key << std::endl << std::endl;
	cout << "--------" << endl;
	//EJERCICIO 5 -----------------------------------------------------------
	float speed = 1.0f, rotSpeed = 2.5f;
	float motionBlurChangeSpeed = 0.01f;
	float focalDistanceSpeed = 1.0f;
	float maxDistanceFactorSpeed = 0.01f;

	float newVal = 0.0f;
	pair<Scene, bool> * auxScene;
	switch (key)
	{
		//MOVIMIENTO ORBITAL
	case 'w':
		radius -= scrollSpeed;
		drawOrbitalCamera();
		break;
	case 's':
		radius += scrollSpeed;
		drawOrbitalCamera();
		break;

		//MOVIMIENTO TECLADO
	/*case 'w':
		scene.camera.SetPosition(scene.camera.GetPosition() + (scene.camera.GetForward() * speed));
		break;
	case 'a':
		scene.camera.SetPosition(scene.camera.GetPosition() - (scene.camera.GetRight() * speed));
		break;
	case 's':
		scene.camera.SetPosition(scene.camera.GetPosition() - (scene.camera.GetForward() * speed));
		break;
	case 'd':
		scene.camera.SetPosition(scene.camera.GetPosition() + (scene.camera.GetRight() * speed));
		break;
	case 'q':
		scene.camera.SetForward(vec3(glm::rotate(mat4(1.0), rotSpeed, vec3(0.0, 1.0, 0.0)) * vec4(scene.camera.GetForward(), 0.0)));
		break;
	case 'e':
		scene.camera.SetForward(vec3(glm::rotate(mat4(1.0), -rotSpeed, vec3(0.0, 1.0, 0.0)) * vec4(scene.camera.GetForward(), 0.0)));
		break;*/

		//VARIABLES
		//Tess level inner
	case 'o': case 'O':
		newVal = scenes[6].first.tessLevelInner + 0.5f;
		if (newVal > 50)	newVal = 50;
		for (size_t i = 0; i < scenes.size(); i++)
			scenes[i].first.tessLevelInner = newVal;
		cout << "TessLevelInner: " << newVal << endl;
		break;

	case 'l': case 'L':
		newVal = scenes[6].first.tessLevelInner - 0.5f;
		if (newVal < 1)	newVal = 1;
		for (size_t i = 0; i < scenes.size(); i++)
			scenes[i].first.tessLevelInner = newVal;

		cout << "TessLevelInner: " << newVal << endl;
		break;

		//Tess level outer
	case 'i': case 'I':
		newVal = scenes[6].first.tessLevelOuter + 0.5f;

		if (newVal > 50.0)	newVal = 50.0;
		for (size_t i = 0; i < scenes.size(); i++)
			scenes[i].first.tessLevelOuter = newVal;

		cout << "TessLevelOuter: " << newVal << endl;
		break;
	case 'k': case 'K':
		newVal = scenes[6].first.tessLevelOuter - 0.5f;

		if (newVal < 1)	newVal = 1;
		for (size_t i = 0; i < scenes.size(); i++)
			scenes[i].first.tessLevelOuter = newVal;


		cout << "TessLevelOuter: " << newVal << endl;
		break;

	case 'u': case 'U':
		newVal = scenes[8].first.displacementAmount + 0.05;

		if (newVal > 1) newVal = 1;
		for (size_t i = 0; i < scenes.size(); i++)
			scenes[i].first.displacementAmount = newVal;
		cout << "Displacement Factor: " << newVal << endl;

		break;

	case 'j': case 'J':
		newVal = scenes[8].first.displacementAmount - 0.05;

		if (newVal < 0) newVal = 0;
		for (size_t i = 0; i < scenes.size(); i++)

			scenes[i].first.displacementAmount = newVal;
		cout << "Displacement Factor: " << newVal << endl;

		break;

	case 'y': case 'Y':
		newVal = scenes[10].first.lodMinTessLevel + 0.5f;
		if (newVal > 50) newVal = 50;
		for (size_t i = 0; i < scenes.size(); i++)
		{
			scenes[i].first.lodMinTessLevel = newVal;
			if (scenes[i].first.lodMaxTessLevel < newVal)
				scenes[i].first.lodMaxTessLevel = newVal;
		}
		cout << "LodMinTessLevel: " << newVal << endl;

		break;
	case 'h': case 'H':
		newVal = scenes[10].first.lodMinTessLevel - 0.5f;
		if (newVal < 1) newVal = 1;
		for (size_t i = 0; i < scenes.size(); i++)
			scenes[i].first.lodMinTessLevel = newVal;
		cout << "LodMinTessLevel: " << newVal << endl;

		break;

	case 't': case 'T':
		newVal = scenes[10].first.lodMaxTessLevel + 0.5f;
		if (newVal > 50) newVal = 50;
		for (size_t i = 0; i < scenes.size(); i++)
			scenes[i].first.lodMaxTessLevel = newVal;

		cout << "LodMaxTessLevel: " << newVal << endl;

		break;
	case 'g': case 'G':
		newVal = scenes[10].first.lodMaxTessLevel - 0.5f;
		if (newVal < 1) newVal = 1;
		for (size_t i = 0; i < scenes.size(); i++)
		{
			scenes[i].first.lodMaxTessLevel = newVal;
			if (scenes[i].first.lodMinTessLevel > newVal)
				scenes[i].first.lodMinTessLevel = newVal;
		}
		cout << "LodMaxTessLevel: " << newVal << endl;

		break;

	case '1':
		auxScene = &scenes[0];
		auxScene->second = !auxScene->second;
		cout << "Escena normal: " << auxScene->second << endl;
		break;

	case '2':
		auxScene = &scenes[2];
		auxScene->second = !auxScene->second;
		cout << "Escena puntos: " << auxScene->second << endl;
		break;

	case '3':
		auxScene = &scenes[3];
		auxScene->second = !auxScene->second;
		cout << "Escena normales: " << auxScene->second << endl;
		break;

	case '4':
		auxScene = &scenes[4];
		auxScene->second = !auxScene->second;
		cout << "Escena normales centrales: " << auxScene->second << endl;
		break;

	case '5':
		auxScene = &scenes[5];
		auxScene->second = !auxScene->second;
		cout << "Escena wireframe: " << auxScene->second << endl;
		break;

	case '6':
		auxScene = &scenes[6];
		auxScene->second = !auxScene->second;
		cout << "Escena triángulos teselación: " << auxScene->second << endl;
		break;

	case '7':
		auxScene = &scenes[7];
		auxScene->second = !auxScene->second;
		cout << "Escena quad teselación: " << auxScene->second << endl;
		break;

	case '8':
		if (!scenes[8].second)
		{
			if (!scenes[9].second)
			{
				scenes[8].second = true;
			}
			else
				scenes[9].second = false;
		}
		else
		{
			scenes[8].second = false;
			scenes[9].second = true;
		}
		cout << "Escenas desplazamiento: "
			<< "\nEn Geometría: " << scenes[8].second
			<< "\nEn Tesellation Evaluation: " << scenes[9].second << endl;
		break;

	case '9':
		if (!scenes[10].second)
		{
			if (!scenes[11].second)
			{
				scenes[10].second = true;
			}
			else
				scenes[11].second = false;
		}
		else
		{
			scenes[10].second = false;
			scenes[11].second = true;
		}
		cout << "Escenas desplazamiento LOD: "
			<< "\nPhong: " << scenes[10].second
			<< "\nWireframe: " << scenes[11].second << endl;
		break;

	default:
		break;
	}

	UpdateCameras(scene.camera);
}

void mouseFunc(int button, int state, int x, int y)
{
	if (state == 0)
		std::cout << "Se ha pulsado el botón ";
	else
		std::cout << "Se ha soltado el botón ";

	if (button == 0) std::cout << "de la izquierda del ratón " << std::endl;
	if (button == 1) std::cout << "central del ratón " << std::endl;
	if (button == 2) std::cout << "de la derecha del ratón " << std::endl;

	std::cout << "en la posición " << x << " " << y << std::endl << std::endl;

	if (button == 0 && state == 0)
	{
		//Se acaba de apretar el botón izquierdo
		std::cout << "Prev cambiado" << std::endl;
		prevV = glm::vec2(x, y);
	}
}

vec3 ClampVector(vec3 vec, float min, float max)
{
	return vec3(clamp(vec.x, min, max), clamp(vec.y, min, max), clamp(vec.z, min, max));
}

void mouseMotionFunc(int x, int y)
{
	//std::cout << "en la posición " << x << " " << y << std::endl << std::endl;

	static glm::vec2 sensitivity(0.03, 0.03);

	glm::vec2 displacement = glm::vec2(x, y) - prevV;
	//std::cout << displacement.x << "||" << displacement.y << std::endl;

	//rot = rot + glm::vec2(displacement.x * sensitivity.x, displacement.y * sensitivity.y);
	rotV += displacement * sensitivity;

	auto clampRot = [](float num)->float { return num > 2 * PI ? 0 : num; };

	rotV = glm::vec2(rotV.x, clamp(rotV.y, -PI*0.5f, PI*0.5f));

	drawOrbitalCamera();

	prevV = glm::vec2(x, y);
	//std::cout << rot.x << "||" << rot.y << std::endl;
	std::cout << radius << std::endl;
}

