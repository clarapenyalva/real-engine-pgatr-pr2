#version 330 core
layout (points) in;
layout (line_strip, max_vertices = 2) out;

uniform mat4 projection;

in vec3 vProjNormal[];

out vec3 gColor;

//Returns position of the vertex in view space
void DrawVertexLine(int index)
{
    gl_Position = projection *  gl_in[index].gl_Position; 
	gColor = vec3(1.0,0.0,0.0);
    EmitVertex();

	vec4 displacedPos = projection * (gl_in[index].gl_Position 
		+ vec4(vProjNormal[index], 0.0)*0.1);
	gl_Position = displacedPos;
	gColor = vec3(1.0,0.0,0.0);
	EmitVertex();
	EndPrimitive();
}

void main() {   
	DrawVertexLine(0);
}  