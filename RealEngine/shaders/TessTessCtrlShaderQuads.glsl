#version 430 core

//uniform float uOuter;
layout(vertices = 4) out;

//in vec3 vColor[];
//out vec3 tcColor[];

uniform float uTessLevelInner;
uniform float uTessLevelOuter;


in vec3 vProjNormal[];

out vec3 tCProjNormal[];

void main()
{
	
	//cu�nto se va a teselar cada parche
	gl_TessLevelInner[0] = uTessLevelInner; //num columnas
	gl_TessLevelInner[1] = uTessLevelInner; //num filas

	gl_TessLevelOuter[0] = uTessLevelOuter;
	gl_TessLevelOuter[1] = uTessLevelOuter;
	gl_TessLevelOuter[2] = uTessLevelOuter;
	gl_TessLevelOuter[3] = uTessLevelOuter;

	//tcColor[gl_InvocationID] = vColor[gl_InvocationID];
	tCProjNormal[gl_InvocationID] = vProjNormal[gl_InvocationID];

	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

}
