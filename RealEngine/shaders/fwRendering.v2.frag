#version 330 core

layout(location = 0) out vec4 fColor;
layout(location = 1) out vec4 fVertex;

in vec3 vPosition;
in vec3 vNormal;
in vec3 vColor;
in vec2 vTexCoord;

uniform sampler2D colorTex;
uniform sampler2D emiTex;


//Par�metros de cada luz
struct LightInfo {
	vec3 lightPos;		// posici�n de la luz
	vec3 intensityL;	// intensidad de la fuente (difusa)
	vec3 dir;			// Direcci�n del foco (S.R. del mundo)
	float cutOffInt;	// �ngulo interior (zona iluminada)
	float cutOffExt;	// �ngulo exterior (zona penumbra)
	float spotExp;		// Exponente del spotlight
};

//Luz ambiental
vec3 intensityA = vec3(0.1f);

//Materiales del objeto
vec3 Ka; //coeficiente ambiental
vec3 Kd; //coeficiente difusa
vec3 Ks; //coeficiente especular
vec3 Ke; //coeficiente emisiva

vec3 N;
float brightness;

vec3 shadePointLight(LightInfo light, vec3 pos);
vec3 shadeDirectionalLight(LightInfo light, vec3 pos);
vec3 shadeSpotLight(LightInfo light, vec3 pos);

void main()
{
	LightInfo light0, light1;

	//Puntual
	light0.lightPos = vec3(0.0f, 0.0f, 0.0f);
	light0.intensityL = vec3(0.6f);

	//Direccional
	light1.intensityL = vec3(0.1f);
	light1.dir = vec3(3.0f, -3.0f, -1.0f);

	//Caracter�sticas del material del objeto
	Ka = vec3(0.5,0.5,0.5);//vColor; 
	Kd = vec3(0.5,0.5,0.5);//vColor;
	Ks = vec3(1.0f);
	Ke = vec3(0.0f);

	//Caracter�sticas del material del objeto con TEXTURA
	//Ka = texture(colorTex, vTexCoord).rgb;
	//Kd = Ka;
	//Ke = texture(emiTex, vTexCoord).rgb;
	//Ke = vec3(0);
	//Ks = texture(specularTex, vTexCoord).rgb;

	brightness = 200.0f;

	N = vNormal;

	vec3 shade1 = shadePointLight(light0, vPosition);
	vec3 shade2 = shadeDirectionalLight(light1, vPosition);

	vec3 color = clamp(intensityA*Ka + shade1 + shade2 + Ke, 0.0f, 1.0f);

	fColor = vec4(color, 0.0f);
}

//Luz puntual
vec3 shadePointLight(LightInfo light, vec3 pos)
{	
	//A este color se le suman las componentes: especular, difusa...
	//Se inicialza en 0 = negro
	vec3 color = vec3(0);

	//difusa
	vec3 L = normalize(light.lightPos - pos);
	color += clamp(light.intensityL*Kd*dot(N,L), 0.0f, 1.0f);

	//especular
	vec3 V = normalize(-pos);
	vec3 R = normalize(reflect(-L,N));
	float Isfactor = clamp(dot(V, R), 0.0, 1.0);
	Isfactor = pow(Isfactor, brightness);
	color += light.intensityL*Ks*Isfactor;

	//emisiva
	color += Ke;
		
	return clamp(color, 0.0f, 1.0f);
}

//Luz direccional
vec3 shadeDirectionalLight(LightInfo light, vec3 pos)
{	
	//A este color se le suman las componentes: especular, difusa...
	//Se inicialza en 0 = negro
	vec3 color = vec3(0);

	//vec3 dir = normalize(light.dir);

	//difusa
	vec3 L = normalize(-light.dir);
	color += clamp(light.intensityL*Kd*dot(N,L), 0.0f, 1.0f);

	//especular
	vec3 V = normalize(-pos);
	vec3 R = normalize(reflect(-L,N));
	float Isfactor = clamp(dot(V, R), 0.0, 1.0);
	Isfactor = pow(Isfactor, brightness);
	//color += light.intensityL*Ks*Isfactor;

	//emisiva
	//color += Ke;
		
	return clamp(color, 0.0f, 1.0f);
}

//Luz focal
vec3 shadeSpotLight(LightInfo light, vec3 pos)
{	
	//A este color se le suman las componentes: especular, difusa...
	//Se inicialza en 0 = negro
	vec3 color = vec3(0);

    //F�rmula extra�da del libro Real-Time Rendering
	vec3 l = normalize(light.lightPos - pos); //direcci�n de la superficie
	float cosAngleS = dot(-l, normalize(light.dir)); //coseno del angulo entre la direcci�n de la superficie y de la luz
	float cosAngleLInt = cos(radians(light.cutOffInt));
	float cosAngleLExt = cos(radians(light.cutOffExt));
	float att;

	if(cosAngleS >= cosAngleLInt) //luz m�xima
	{	
		att = 1.0f;
	}
	else if(cosAngleS > cosAngleLExt)
	{
		att = pow((cosAngleS - cosAngleLExt) / (cosAngleLInt-cosAngleLExt), light.spotExp);
	}
	else
		att = 0.0f;


	//difusa
	vec3 L = normalize(light.lightPos - pos);
	color += att * clamp(light.intensityL*Kd*dot(N,L), 0.0f, 1.0f);

	//especular
	vec3 V = normalize(-pos);
	vec3 R = normalize(reflect(-L,N));
	float Isfactor = clamp(dot(V, R), 0.0, 1.0);
	Isfactor = pow(Isfactor, brightness);
	color += att * light.intensityL*Ks*Isfactor;

	return clamp(color, 0.0f, 1.0f);
}