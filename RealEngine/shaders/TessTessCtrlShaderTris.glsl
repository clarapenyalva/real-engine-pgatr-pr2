#version 430 core

layout (vertices = 3) out;

in vec3 vProjNormal[];

uniform float uTessLevelInner;
uniform float uTessLevelOuter;

out vec3 tCProjNormal[];

void main(void)
{

        gl_TessLevelInner[0] = uTessLevelInner;
        gl_TessLevelOuter[0] = uTessLevelOuter;
        gl_TessLevelOuter[1] = uTessLevelOuter;
        gl_TessLevelOuter[2] = uTessLevelOuter;
    
	
	tCProjNormal[gl_InvocationID] = vProjNormal[gl_InvocationID];

    gl_out[gl_InvocationID].gl_Position =
        gl_in[gl_InvocationID].gl_Position;
}