#version 330 core
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec2 tEUVs[];
in vec3 tEProjNormal[];
in vec3 tEPos[];

out vec2 gUVs;
out vec3 gPos;
out vec3 gNormal;

void EmitPoint(int index)
{
	gUVs = tEUVs[index];
	gPos = tEPos[index];
	gNormal = tEProjNormal[index];
	gl_Position = gl_in[index].gl_Position;
	EmitVertex();
}

void main() {
	EmitPoint(0);
	EmitPoint(1);
	EmitPoint(2);
	EndPrimitive();
}

