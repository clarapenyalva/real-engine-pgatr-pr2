#version 330 core
layout(triangles) in;
layout(line_strip, max_vertices = 6) out;

uniform mat4 projection;

in vec3 vProjNormal[];

void EmitPoint(int index)
{
	float offset = 0.001;

	gl_Position = projection * 
	(gl_in[index].gl_Position + vec4(vProjNormal[index], 0.0) * offset);
	EmitVertex();
}

void main() {
	EmitPoint(0);
	EmitPoint(1);
	EmitPoint(2);

	EndPrimitive();
}