#version 330 core

in vec3 inPos;	
in vec3 inColor;
in vec2 inTexCoord;
in vec3 inNormal;

uniform mat4 modelViewProj;
uniform mat4 modelView;
uniform mat4 normal;

out vec3 vPosition;
out vec3 vNormal;
out vec3 vColor;
out vec2 vTexCoord;

out vec3 gPosition;
out vec3 gNormal;
out vec3 gColor;
out vec2 gTexCoord;


void main()
{
	vColor = inColor;
	vTexCoord = inTexCoord;
	vNormal = (normal * vec4(inNormal, 0.0)).xyz;
	vPosition = (modelView * vec4(inPos, 1.0)).xyz;
	
	gl_Position =  modelViewProj * vec4 (inPos,1.0);
}
