#version 430 core

layout (vertices = 3) out;

uniform float uTessLevelInner;
uniform float uTessLevelOuter;

in vec3 vProjNormal[];
in vec2 vUVs[];
in vec3 vPos[];

out vec3 tCProjNormal[];
out vec2 tCUVs[];
out vec3 tCPos[];

void main(void)
{
    gl_TessLevelInner[0] = uTessLevelInner;
    gl_TessLevelOuter[0] = uTessLevelOuter;
    gl_TessLevelOuter[1] = uTessLevelOuter;
    gl_TessLevelOuter[2] = uTessLevelOuter;
	
	tCProjNormal[gl_InvocationID] = vProjNormal[gl_InvocationID];
	tCUVs[gl_InvocationID] = vUVs[gl_InvocationID];
	tCPos[gl_InvocationID] = vPos[gl_InvocationID];

    gl_out[gl_InvocationID].gl_Position =
        gl_in[gl_InvocationID].gl_Position;
}