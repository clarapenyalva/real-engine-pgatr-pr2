#version 330 core

layout(location = 0) out vec4 outColor;

in vec3 gColor;

void main()
{
	outColor = vec4(gColor,1.0);
}