#version 330 core
layout (points) in;
layout (points, max_vertices = 1) out;

uniform mat4 projection;

in vec3 vProjNormal[];//Per evitar el z-fighting

void main() {    
    gl_Position = projection*( gl_in[0].gl_Position + vec4(vProjNormal[0], 0.0) * 0.005);
    EmitVertex();
    EndPrimitive();
}  