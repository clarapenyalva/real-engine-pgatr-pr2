#version 400 core

in vec3 inPos;	
in vec3 inColor;
in vec2 inTexCoord;
in vec3 inNormal;

uniform mat4 modelViewProj;
uniform mat4 modelView;
uniform mat4 normal;
uniform mat4 projection;

//out vec3 vColor;
out vec3 vProjNormal;
out vec2 vUVs;
out vec3 vPos;

void main()
{
	//vColor = inColor;
	vPos = (modelView * vec4(inPos, 1.0)).xyz;
	vProjNormal =  normalize((normal * vec4(inNormal, 0.0)).xyz);
	vUVs = inTexCoord;

	gl_Position = vec4 (inPos, 1.0);
}
