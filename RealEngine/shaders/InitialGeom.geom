#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec3 vColor[];
in vec3 vPosition[];
in vec3 vNormal[];
in vec2 vTexCoord[];

out vec3 gColor;
out vec3 gPosition;
out vec3 gNormal;
out vec2 gTexCoord; 

void EmitPoint(int index)
{
	gColor = vColor[index];
	gPosition = vPosition[index];
	gNormal = vNormal[index];
	gTexCoord = vTexCoord[index];

	gl_Position = gl_in[index].gl_Position;
	EmitVertex();
}

void main() {	

	EmitPoint(0);
	EmitPoint(1);
	EmitPoint(2);

	EndPrimitive();
}