#version 330 core
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outVertex;

in vec3 pos;
in vec3 norm;

void main()
{

	outColor = vec4(1.0, 0.0, 0.0, 1.0);   

	outVertex = vec4(pos, 1.0);
}

