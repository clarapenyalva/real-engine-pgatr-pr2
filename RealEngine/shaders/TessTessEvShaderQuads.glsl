#version 430 core

layout (quads) in;

//in vec3 tcColor[];

uniform mat4 modelViewProj;
uniform mat4 modelView;
uniform mat4 normal;

//out vec3 teColor;

in vec3 tCProjNormal[];

out vec3 tEProjNormal;

void main(void)
{
	 vec3 n1 = mix(tCProjNormal[0], tCProjNormal[1], gl_TessCoord.x);
	 vec3 n2 = mix(tCProjNormal[2], tCProjNormal[3], gl_TessCoord.x);
	 tEProjNormal = mix(n1, n2, gl_TessCoord.y);


	 vec3 p0 = vec3(gl_in[0].gl_Position);
	 vec3 p1 = vec3(gl_in[1].gl_Position);
	 vec3 p2 = vec3(gl_in[2].gl_Position);
	 vec3 p3 = vec3(gl_in[3].gl_Position);

	 //coordenadas paramétricas (u,v)
	 float u = gl_TessCoord.x;
	 float v = gl_TessCoord.y;

	 vec3 l1 = p1-p0;
	 vec3 l2 = p3-p0;
	 vec3 p = l1*u + l2*v + p0;
	 //p = normalize(p);
	 gl_Position = modelViewProj * vec4(p, 1.0);

}