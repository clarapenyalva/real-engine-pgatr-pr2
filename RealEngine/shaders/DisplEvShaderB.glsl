#version 400 core

layout (triangles) in;

in vec3 tCProjNormal[];
in vec2 tCUVs[];
in vec3 tCPos[];

uniform mat4 projection;
uniform sampler2D displTex;
uniform float uDisplacementAmount;

out vec2 tEUVs;
out vec3 tEProjNormal;
out vec3 tEPos;

void main(void)
{
	/*Dos opciones:
	 a) Calcular cada punto extruido y hacer interpolaci�n lineal/esf�rica entre ellos.
	 b) Calcular los puntos sin extruir, y s�lo extruir el final

	 Usaremos la b)
	*/

	vec3 normal = (gl_TessCoord.x * tCProjNormal[0]) +
                  (gl_TessCoord.y * tCProjNormal[1]) +
                  (gl_TessCoord.z * tCProjNormal[2]);

    vec2 UVs = (gl_TessCoord.x * tCUVs[0]) +
                  (gl_TessCoord.y * tCUVs[1]) +
                  (gl_TessCoord.z * tCUVs[2]);

	vec3 worldPos = (gl_TessCoord.x * tCPos[0]) +
                  (gl_TessCoord.y * tCPos[1]) +
                  (gl_TessCoord.z * tCPos[2]);

	vec4 offsetVec = texture(displTex, UVs);
	float offset =  0.2 * offsetVec.g + 0.2 * offsetVec.r -  0.4 * offsetVec.b;
		
	vec4 newPos = vec4( worldPos, 1.0) + vec4(normal, 0.0) 
		* offset * uDisplacementAmount;

	tEUVs = UVs;

	tEProjNormal = (gl_TessCoord.x * tCProjNormal[0]) +
		(gl_TessCoord.y * tCProjNormal[1]) +
		(gl_TessCoord.z * tCProjNormal[2]);

	tEPos = (gl_TessCoord.x * tCPos[0]) +
		(gl_TessCoord.y * tCPos[1]) +
		(gl_TessCoord.z * tCPos[2]);

	gl_Position = projection * newPos;
}