#version 330 core
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform sampler2D displTex;
uniform mat4 projection;
uniform float uDisplacementAmount;

in vec2 tEUVs[];
in vec3 tEProjNormal[];
in vec3 tEPos[];

out vec2 gUVs;
out vec3 gPos;
out vec3 gNormal;

void EmitPoint(int index)
{
	vec4 offsetVec = texture(displTex, tEUVs[index]);
	float offset =  0.2 * offsetVec.g + 0.2 * offsetVec.r -  0.4 * offsetVec.b;
		
	vec4 newPos = vec4( tEPos[index], 1.0) +
		vec4(tEProjNormal[index], 0.0) * offset * uDisplacementAmount;

	gUVs = tEUVs[index];
	gPos = newPos.xyz;
	gNormal = tEProjNormal[index];
	gl_Position = projection * newPos;
	EmitVertex();
}

void main() {
	EmitPoint(0);
	EmitPoint(1);
	EmitPoint(2);

	EndPrimitive();
}

