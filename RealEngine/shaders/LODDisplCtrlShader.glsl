#version 430 core

layout (vertices = 3) out;

uniform mat4 modelView;
uniform mat4 modelViewProj;
uniform float uLodMinTessLevel;
uniform float uLodMaxTessLevel;

in vec3 vProjNormal[];
in vec2 vUVs[];
in vec3 vPos[];

out vec3 tCProjNormal[];
out vec2 tCUVs[];
out vec3 tCPos[];

const float uMinDepth = 0.5;
const float uMaxDepth = 5.0;


float level (in vec4 pos)
{
	vec4 p = modelView * pos;
	float depth = clamp( (abs(p.z) - uMinDepth) / (uMaxDepth - uMinDepth), 0.0, 1.0);
	return mix(uLodMaxTessLevel, uLodMinTessLevel, depth );
}

void main(void)
{
	float tessLevel;
	float tessLevelIn;

	vec4 p0 = gl_in[0].gl_Position;
	vec4 p1 = gl_in[1].gl_Position;
	vec4 p2 = gl_in[2].gl_Position;
	vec4 posCentr = (p0 + p1 + p2) / 3; 
	tessLevel = level(posCentr);
	
	//cu�nto se va a teselar cada parche
	gl_TessLevelInner[0] = tessLevel;

	//Interpolaci�n al punto medio para que los lados concidentes
	//de distintas facetas tengan el mismo nivel de teselaci�n
	tessLevel = level(mix(p1, p2, 0.5)); 
	gl_TessLevelOuter[0] = tessLevel;

	tessLevel = level(mix(p0, p2, 0.5));
	gl_TessLevelOuter[1] = tessLevel;

	tessLevel = level(mix(p0, p1, 0.5));
	gl_TessLevelOuter[2] = tessLevel;
	
	tCProjNormal[gl_InvocationID] = vProjNormal[gl_InvocationID];
	tCUVs[gl_InvocationID] = vUVs[gl_InvocationID];
	tCPos[gl_InvocationID] = vPos[gl_InvocationID];

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}