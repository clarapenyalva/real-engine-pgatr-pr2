#version 400 core

layout (triangles) in;

in vec3 tCProjNormal[];
in vec2 tCUVs[];
in vec3 tCPos[];


out vec3 tEProjNormal;
out vec2 tEUVs;
out vec3 tEPos;

void main(void)
{
	tEProjNormal = (gl_TessCoord.x * tCProjNormal[0]) +
                  (gl_TessCoord.y * tCProjNormal[1]) +
                  (gl_TessCoord.z * tCProjNormal[2]);

    tEUVs = (gl_TessCoord.x * tCUVs[0]) +
                  (gl_TessCoord.y * tCUVs[1]) +
                  (gl_TessCoord.z * tCUVs[2]);

	tEPos = (gl_TessCoord.x * tCPos[0]) +
                  (gl_TessCoord.y * tCPos[1]) +
                  (gl_TessCoord.z * tCPos[2]);


    gl_Position = (gl_TessCoord.x * gl_in[0].gl_Position) +
                  (gl_TessCoord.y * gl_in[1].gl_Position) +
                  (gl_TessCoord.z * gl_in[2].gl_Position);
}