#version 400 core

layout (triangles) in;

in vec3 tCProjNormal[];

out vec3 tEProjNormal;

void main(void)
{
	tEProjNormal = (gl_TessCoord.x * tCProjNormal[0]) +
                  (gl_TessCoord.y * tCProjNormal[1]) +
                  (gl_TessCoord.z * tCProjNormal[2]);

    gl_Position = (gl_TessCoord.x * gl_in[0].gl_Position) +
                  (gl_TessCoord.y * gl_in[1].gl_Position) +
                  (gl_TessCoord.z * gl_in[2].gl_Position);
}