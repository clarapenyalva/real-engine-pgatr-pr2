#version 330 core
layout(triangles) in;
layout(line_strip, max_vertices = 3) out;

uniform sampler2D displTex;
uniform mat4 projection;
uniform float uDisplacementAmount;
//JMP: No me conven� perqu� estem dibuixant els edges coincidents dues voltes
in vec3 tEProjNormal[];//Per evitar el z-fighting
in vec2 tEUVs[];
in vec3 tEPos[];


out vec2 gUVs;

void EmitPoint(int index)
{
	vec4 offsetVec = texture(displTex, tEUVs[index]);
	float offset = offsetVec.r;
		
	vec4 newPos = vec4( tEPos[index], 1.0) + 
		vec4(tEProjNormal[index], 0.0) * (offset + 0.01) * uDisplacementAmount;

	gUVs = tEUVs[index];
	gl_Position = projection * newPos;
	EmitVertex();
}

void main() {
	EmitPoint(0);
	EmitPoint(1);
	EmitPoint(2);

	EndPrimitive();
}