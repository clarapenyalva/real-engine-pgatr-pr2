#version 330 core
layout (triangles) in;
layout (line_strip, max_vertices = 2) out;

uniform mat4 projection;


in vec3 vProjNormal[];

out vec3 gColor;

void main() {  

	vec4 p0 = gl_in[0].gl_Position;
	vec4 p1 = gl_in[1].gl_Position;
	vec4 p2 = gl_in[2].gl_Position;

	vec4 n0 = vec4(vProjNormal[0],0.0);
	vec4 n1 =  vec4(vProjNormal[1],0.0);
	vec4 n2 =  vec4(vProjNormal[2],0.0);

	vec4 midPos =(p0 + p1 + p2)/3.0;
	vec4 midNorm = (n0 + n1 + n2)/3.0;

	gl_Position = projection * vec4(midPos.xyz,1.0);
	gColor = vec3(0.0, 1.0, 1.0);
	EmitVertex();

	gl_Position = projection * (vec4(midPos.xyz,1.0) 
		+ vec4(normalize(midNorm).xyz, 1.0) * 0.05);
	gColor = vec3(0.0, 1.0, 1.0);
	EmitVertex();
	EndPrimitive();
}  