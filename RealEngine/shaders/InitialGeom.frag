#version 330 core

layout(location = 0) out vec4 fColor;
layout(location = 1) out vec4 fVertex;

in vec3 gPosition;
in vec3 gNormal;
in vec3 gColor;
in vec2 gTexCoord;

uniform sampler2D colorTex;
uniform sampler2D emiTex;


//Par�metros de cada luz
struct LightInfo {
	vec3 lightPos;		// posici�n de la luz
	vec3 intensityL;	// intensidad de la fuente (difusa)
	vec3 dir;			// Direcci�n del foco (S.R. del mundo)
	float cutOffInt;	// �ngulo interior (zona iluminada)
	float cutOffExt;	// �ngulo exterior (zona penumbra)
	float spotExp;		// Exponente del spotlight
};

//Luz ambiental
vec3 intensityA = vec3(0.1f);

//Materiales del objeto
vec3 Ka; //coeficiente ambiental
vec3 Kd; //coeficiente difusa
vec3 Ks; //coeficiente especular
vec3 Ke; //coeficiente emisiva

vec3 N;
float brightness;

vec3 shadePointLight(LightInfo light, vec3 pos);
vec3 shadeDirectionalLight(LightInfo light, vec3 pos);
vec3 shadeSpotLight(LightInfo light, vec3 pos);

void main()
{
	LightInfo light0, light1, light2;

	//Puntual
	light0.lightPos = vec3(-2.0f, 2.0f, 0.0f);
	light0.intensityL = vec3(0.5f);

	//Direccional
	light1.intensityL = vec3(0.1f);
	light1.dir = vec3(3.0f, -3.0f, -1.0f);

	//Focal
	light2.lightPos = vec3(0.0f, 0.0f, -2.0f);
	light2.intensityL = vec3(0.5f);
	light2.dir = vec3(0.0f, 0.0f, -1.0f);
	light2.cutOffInt = 5.0f;
	light2.cutOffExt = 10.0f;
	light2.spotExp = 2.0f;

	//Caracter�sticas del material del objeto
//	Ka = gColor; 
//	Kd = gColor;
//	Ks = vec3(1.0f);
//	Ke = vec3(0.0f);
//	brightness = 50.0f;

	//Caracter�sticas del material del objeto con TEXTURA
	Ka = texture(colorTex, gTexCoord).rgb;
	Kd = Ka;
	Ke = texture(emiTex, gTexCoord).rgb;
	//Ke = vec3(0);
	//Ks = texture(specularTex, gTexCoord).rgb;
	Ks = vec3 (0.5);
	brightness = 500.0f;

	N = gNormal;

	vec3 shade1 = shadePointLight(light0, gPosition);
	vec3 shade2 = shadeDirectionalLight(light1, gPosition);
	vec3 shade3 = shadeSpotLight(light2, gPosition);

	vec3 color = clamp(intensityA*Ka + shade1 + shade2 + shade3 + Ke, 0.0f, 1.0f);

	fColor = vec4(color, 0.0f);
}

//Luz puntual
vec3 shadePointLight(LightInfo light, vec3 pos)
{	
	//A este color se le suman las componentes: especular, difusa...
	//Se inicialza en 0 = negro
	vec3 color = vec3(0);

	//difusa
	vec3 L = normalize(light.lightPos - pos);
	color += clamp(light.intensityL*Kd*dot(N,L), 0.0f, 1.0f);

	//especular
	vec3 V = normalize(-pos);
	vec3 R = normalize(reflect(-L,N));
	float Isfactor = clamp(dot(V, R), 0.0, 1.0);
	Isfactor = pow(Isfactor, brightness);
	color += light.intensityL*Ks*Isfactor;

	//emisiva
	color += Ke;
		
	return clamp(color, 0.0f, 1.0f);
}

//Luz direccional
vec3 shadeDirectionalLight(LightInfo light, vec3 pos)
{	
	//A este color se le suman las componentes: especular, difusa...
	//Se inicialza en 0 = negro
	vec3 color = vec3(0);

	//vec3 dir = normalize(light.dir);

	//difusa
	vec3 L = normalize(-light.dir);
	color += clamp(light.intensityL*Kd*dot(N,L), 0.0f, 1.0f);

	//especular
	vec3 V = normalize(-pos);
	vec3 R = normalize(reflect(-L,N));
	float Isfactor = clamp(dot(V, R), 0.0, 1.0);
	Isfactor = pow(Isfactor, brightness);
	//color += light.intensityL*Ks*Isfactor;

	//emisiva
	//color += Ke;
		
	return clamp(color, 0.0f, 1.0f);
}

//Luz focal
vec3 shadeSpotLight(LightInfo light, vec3 pos)
{	
	//A este color se le suman las componentes: especular, difusa...
	//Se inicialza en 0 = negro
	vec3 color = vec3(0);

    //F�rmula extra�da del libro Real-Time Rendering
	vec3 l = normalize(light.lightPos - pos); //direcci�n de la superficie
	float cosAngleS = dot(-l, normalize(light.dir)); //coseno del angulo entre la direcci�n de la superficie y de la luz
	float cosAngleLInt = cos(radians(light.cutOffInt));
	float cosAngleLExt = cos(radians(light.cutOffExt));
	float att;

	if(cosAngleS >= cosAngleLInt) //luz m�xima
	{	
		att = 1.0f;
	}
	else if(cosAngleS > cosAngleLExt)
	{
		att = pow((cosAngleS - cosAngleLExt) / (cosAngleLInt-cosAngleLExt), light.spotExp);
	}
	else
		att = 0.0f;


	//difusa
	vec3 L = normalize(light.lightPos - pos);
	color += att * clamp(light.intensityL*Kd*dot(N,L), 0.0f, 1.0f);

	//especular
	vec3 V = normalize(-pos);
	vec3 R = normalize(reflect(-L,N));
	float Isfactor = clamp(dot(V, R), 0.0, 1.0);
	Isfactor = pow(Isfactor, brightness);
	color += att * light.intensityL*Ks*Isfactor;

	return clamp(color, 0.0f, 1.0f);
}