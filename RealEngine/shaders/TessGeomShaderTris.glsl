#version 330 core
layout(triangles) in;
layout(line_strip, max_vertices = 4) out;

uniform mat4 projection;
uniform mat4 modelView;

in vec3 tEProjNormal[];

void EmitPoint(int index)
{
	vec4 worldPos = modelView * gl_in[index].gl_Position;

	gl_Position = projection * (
		worldPos + vec4(tEProjNormal[index], 0.0) * 0.01);
	EmitVertex();
}

void main() {
	EmitPoint(0);
	EmitPoint(1);
	EmitPoint(2);
	EmitPoint(0);

	EndPrimitive();
}

