# Real Engine, real-time render engine
RealEngine is a real-time render engine in preliminary state. 

* It has been developed to facilitate the performance of different tasks of the Master's Degree in Computer Graphics from Rey Juan Carlos University (Madrid).

* Developed using C++, OpenGL and GLSL.

Tasks performed:
* Basic architecture structured in classes with C++.
* Vertex and fragment shaders and Phong Lighting.
* Introduction of geometry shaders and drawing of vertices and facets and their normals.
* Introduction of tessellation shaders and control of levels of detail in the tessellation stage on basic primitives.
* Displacement mapping. This has been done in two ways: using the geometry phase and using the tessellation evaluation phase.

(you can view the document "MemoriaP2-PGATR.pdf")

[Developed in 2019]

## Credits
* Clara Peñalva Carbonell [[webpage](https://clarapenyalva.com)] [[GitLab profile](https://gitlab.com/clarapenyalva)]
* José María Pizana García  [[webpage](https://jmpizana.com)] [[GitLab profile](https://gitlab.com/jmpizanagarcia)]